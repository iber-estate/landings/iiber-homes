/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    screens: {
      sm: { max: "639px" },
      xl: { min: "1280px" },
      // => @media (max-width: 639px) { ... }
    },
    extend: {
      colors: {
        primary: {
          light: "#C9DBD8", // Lighter shade of primary color
          DEFAULT: "#559D88", // Primary color
          dark: "#1B5C6B", // Darker shade of primary color
        },
        black: "#222",
        lightblueone: "#DEE6F0",
        lightbluetwo: "#DAE8EC",
        lightbluethree: "#E8EFEA",
        blue: "#A6CBDD",
        gray: "#505253",
        darkAquamarine: "#104551",
        beige: "#FEFBF6",
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      fontSize: {
        sm: "0.8rem",
        base: "1rem",
        lg: "1.25rem",
        xl: "1.5rem",
        "2xl": "2rem",
        "3xl": "4rem",
      },
      fontFamily: {
        satoshi: ["var(--font-satoshi)"],
        onest: ["var(--font-onest)"],
      },
      borderRadius: {
        DEFAULT: "8px",
      },
      grayscale: {
        80: "80%",
      },
    },
  },
  plugins: [],
};
