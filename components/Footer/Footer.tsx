"use client";
import Image from "next/image";
import { Typography } from "@/components/Typography";
import Link from "next/link";
import { twMerge } from "tailwind-merge";

const Links = ({ classname }: { classname: string }) => (
  <div className={classname}>
    <Typography type="body" className="mb-[8px] sm:hidden">
      Iber Home & Invest
    </Typography>
    <Typography
      type="subtitle2"
      className="mb-[8px] hidden sm:block sm:w-[144px] text-black"
    >
      Iber Home & Invest
    </Typography>
    <div className="flex gap-[16px]">
      <Link href="https://www.instagram.com/iconsult_iber/" target="_blank">
        <Image
          src="assets/inst-outlined.svg"
          alt="inst"
          width={32}
          height={32}
        />
      </Link>
      <Link href="https://www.linkedin.com/company/iberrest/" target="_blank">
        <Image src="assets/in-outlined.svg" alt="in" width={32} height={32} />
      </Link>
      <Link href="https://t.me/IberhomesRu" target="_blank">
        <Image src="assets/tg-outlined.svg" alt="tg" width={32} height={32} />
      </Link>
    </div>
  </div>
);

export function Footer() {
  return (
    <section className="bg-primary-light w-fit xl:w-[100vw]">
      <div className="pt-[80px] pb-[80px] gridWrapper sm:gap-0 sm:pb-[28px] sm:pt-[60px]">
        <div className="col-span-2 sm:col-span-6">
          <Link href="/">
            <Image
              src="/assets/logo.svg"
              width={67}
              height={44}
              alt="logo-white"
              className="mb-[18px]"
            />
          </Link>
          <Typography type="body" className="text-black">
            Legal Address
          </Typography>
          <Typography type="body" className="text-black">
            IBER HOMES PTE. LTD.
          </Typography>
          <Typography type="body" className="text-black">
            68 Circular Road, Singapore, 049422
          </Typography>
          <div className="flex pt-[16px] sm:min-w-[0] sm:w-auto min-w-[1280px] max-w-[1440px] mx-auto pb-[16px] gap-[12px] w-full sm:justify-start sm:px-[28px]">
            <Link href="/assets/privacy_policy.pdf" target="_blank">
              <Typography type="body" className="text-black">
                Terms and Conditions
              </Typography>
            </Link>
            <Link href="/assets/privacy_policy.pdf" target="_blank">
              <Typography type="body" className="text-black">
                Privacy Policy
              </Typography>
            </Link>
          </div>
        </div>
        <div className="col-span-2 flex justify-center sm:col-span-6 sm:justify-start sm:mb-[20px]">
          <div className="flex flex-col gap-[24px] sm:gap-[20px]">
            <Typography type="body" className="text-black">
              <span className="mb-[16px] block">Services: </span>
              <ul className="list-disc flex flex-col gap-[8px]">
                <li>
                  International real estate - access to global real estate
                  markets.
                </li>
                <li>
                  Investment opportunities - discovering high-potential
                  projects.
                </li>
                <li>
                  Investment focus - introducing your project to investors.
                </li>
                <li>
                  Local experts - market insight, legal and bureaucratic
                  support.
                </li>
                <li>
                  Lifestyle Around Coastal Regions (coming soon) - introduction
                  to coastal living.
                </li>
                <li>
                  International Sales - expand your reach and increase revenue.
                </li>
              </ul>
            </Typography>
          </div>
        </div>
        <div className="col-span-2 flex justify-center sm:col-span-6 sm:justify-start sm:mb-[32px]">
          <div className="flex flex-col gap-[24px] sm:gap-[20px]">
            <Typography type="body" className="text-black">
              Contacts
            </Typography>
            <Typography type="body" className="text-black">
              <span>Email: </span>
              <u>
                <Link href="mailto:info@iber.homes">info@iber.homes</Link>
              </u>
            </Typography>
            <Typography type="body" className="text-black">
              <span>Telegram: </span>
              <span className="underline">
                <Link href="https://t.me/iberhomes">@iberhomes</Link>
              </span>
            </Typography>
            <Typography type="body" className="text-black">
              <span>Book a Call: </span>
              <span className="underline">
                <Link href="https://calendly.com/iberhomes">
                  Schedule a Call with Us
                </Link>
              </span>
            </Typography>
            <Typography type="body" className="text-black">
              <span>Pay via Stripe: </span>
              <span className="underline">
                <Link href="https://buy.stripe.com/4gw6pH5m1ex82nS8ww">
                  Visa, Mastercard, etc.
                </Link>
              </span>
            </Typography>
          </div>
        </div>
        <Links classname="hidden sm:block col-span-6" />
      </div>
    </section>
  );
}
