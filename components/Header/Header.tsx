"use client";
import React, { useRef, useState } from "react";
import { Menu } from "@/components/Menu";
import Button from "@/components/Button";
import logo from "@/public/assets/logo.svg";
import Link from "next/link";
import Image from "next/image";
import { useStore } from "@/StoreContext";

export const Header = () => {
  const [requestOpen, setRequestOpen] = useState(false);
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const unitsButton = useRef<HTMLDivElement>(null);

  const toggleMobileMenu = () => {
    setMobileMenuOpen(!mobileMenuOpen);
  };

  return (
    <header>
      <div
        className={`gridWrapper m-auto pt-[28px] sm:mb-0 sm:py-[48px] sm:mb-[12px]`}
      >
        <div className="col-span-6 flex items-center justify-between sm:flex-col">
          <div
            className={`${
              mobileMenuOpen && "sm:mb-[32px]"
            } sm:self-start sm:flex sm:justify-between sm:w-full `}
          >
            <Link href="/">
              <Image src={logo} alt="logo" />
            </Link>
            <button className="hidden sm:block" onClick={toggleMobileMenu}>
              {mobileMenuOpen ? (
                <Image
                  src="/assets/cross.svg"
                  alt="cross"
                  width={24}
                  height={24}
                />
              ) : (
                <Image
                  src="/assets/burger.svg"
                  alt="burger"
                  width={48}
                  height={24}
                />
              )}
            </button>
          </div>
          <div className={mobileMenuOpen ? "sm:block" : "sm:hidden"}>
            <Menu />
          </div>
          <div
            className={`flex items-center sm:gap-[16px] sm:justify-center ${
              mobileMenuOpen ? "sm:flex-col" : "sm:hidden"
            }`}
            ref={unitsButton}
          >
            <Link href="https://app.iber.homes/?auth=open">
              <Button
                className="mr-4 sm:mr-0 min-w-[110px] sm:w-[115px] sm:px-[24px]"
                variant="primary"
                color="light"
                onClick={() => {
                  setRequestOpen(true);
                }}
              >
                Log in
              </Button>
            </Link>
            <Link href="https://app.iber.homes/?auth=open">
              <Button
                className="mr-4  sm:mr-0 min-w-[110px] sm:w-[115px] sm:px-[24px]"
                variant="primary"
                onClick={() => {
                  setRequestOpen(true);
                }}
              >
                Sign up
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </header>
  );
};
