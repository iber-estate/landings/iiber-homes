import { MenuItem } from "@/components/MenuItem";

interface MenuProps {
  containerClassName?: string;
  children?: React.ReactNode;
}

const privateMenu = [
  {
    name: "Buy",
    link: "/search",
  },
  {
    name: "Locations",
    link: "/locations",
  },
  {
    name: "Categories",
    link: "/categories",
  },
  {
    name: "About",
    link: "/about",
  },
];

const publicMenu = [
  {
    name: "About",
    link: "/#about",
  },
  {
    name: "Advantages",
    link: "/#advantages",
  },
  {
    name: "FAQ",
    link: "/#faq",
  },
];

export const Menu: React.FC<MenuProps> = () => {
  const user = null;
  const menuLinks = user ? privateMenu : publicMenu;
  return (
    <ul className="flex justify-center space-x-[48px] sm:flex-col sm:items-center sm:space-x-0 sm:gap-y-[8px] sm:mb-[32px]">
      {menuLinks.map((item) => (
        <MenuItem link={item.link} key={item.link}>
          {item.name}
        </MenuItem>
      ))}
    </ul>
  );
};
