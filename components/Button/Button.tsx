import cn from "classnames";
import Image from "next/image";
import { twMerge } from "tailwind-merge";

type ButtonProps = {
  variant?: "primary" | "transparent" | "text";
  color?: "grey" | "default" | "white" | "light";
  size?: "base" | "md" | "sm";
  children?: React.ReactNode;
  className?: string;
  iconLeft?: string;
  loading?: boolean;
  onClick?: () => void;
  type?: "button" | "submit" | "reset";
};

const Button: React.FC<ButtonProps> = (props) => {
  const {
    variant = "primary",
    size = "base",
    color = "default",
    children,
    className,
    iconLeft,
    loading,
    onClick,
    type,
  } = props;

  const colorStyles = {
    default: {
      primary: `
        bg-primary-dark
        border border-[2px] border-primary-dark
        text-white
        font-[200]
        hover:bg-primary
        hover:border hover:border-primary-light hover:border-[2px]
        `,
      transparent: `
        text-primary
        border-primary
        bg-transparent
        hover:bg-blue-100
        `,
      text: "text-blue-600",
    },
    light: {
      primary: `
        bg-primary-light
        border border-[2px] border-primary-light
        text-primary-dark
        font-[200]
        hover:bg-primary hover:text-white 
        hover:border hover:border-primary-light hover:border-[2px]
        `,
      transparent: "",
      text: "",
    },
    grey: {
      primary: "bg-gray-600 text-white hover:bg-gray-700",
      transparent: "text-slate-600 border-slate-600 hover:bg-gray-100",
      text: "text-gray-600",
    },
    white: {
      primary: "bg-beige text-primary-dark hover:bg-gray-200",
      transparent: "text-white border-white",
      text: "text-gray-600",
    },
  };

  const sizeStyles = {
    base: `py-[10px]
     px-[24px]
     rounded-[8px]
     text-base
     line-height-[28px]
     min-w-[160px]
     sm:min-w-[0px]
     w-auto
     sm:text-[14px] 
     sm:leading-[16px] 
     sm:px-[32px] 
     sm:py-[12px]`,
    sm: "py-[6px] px-[16px] rounded-[6px] text-sm line-height-[20px]",
    md: "py-[12px] px-[24px] rounded-[6px] text-base line-height-[24px]",
  };

  return (
    <button
      onClick={onClick}
      disabled={loading}
      type={type}
      className={twMerge(
        "flex gap-[8px] items-center justify-center disabled:opacity-50 font-onest",
        sizeStyles[size],
        colorStyles[color][variant],
        className,
      )}
    >
      {iconLeft && <Image src={iconLeft} alt="Picture of the author" />}
      <span>{loading ? "Loading..." : children}</span>
    </button>
  );
};

export default Button;
