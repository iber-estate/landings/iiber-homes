import React from "react";
import Accordeon from "@/components/Accordion/Accordion";
import { Typography } from "@/components/Typography";

const accordionItems = [
  {
    title: "What is the specific value your platform for me?",
    content:
      "Our platform provides real estate agents with access to the international market, expanding the range of properties available in various countries for your clients. This enhances the value and diversity of your offerings, making your work more professional and informative with detailed property descriptions and purchase conditions, as well as a better understanding of different countries, regions, cities, and neighborhoods.",
  },
  {
    title: "Can your platform help me connect with clients?",
    content:
      "While our platform does not directly connect agents and clients, it provides you with the tools and resources to serve your clients effectively. You can create and manage property listings, making it easier to showcase properties to potential buyers.",
  },
  {
    title: "Are there any subscription fees, transaction fees, or commissions?",
    content:
      "We believe in transparency. You will have unrestricted access to all properties in our database at no cost. However, should you require additional property checks or transaction assistance, our legal experts are available to provide support. The pricing for these supplementary services can be discussed on an individual basis to best meet your specific needs.",
  },
  {
    title: "What kind of support do you offer to real estate agents?",
    content:
      "We provide comprehensive support for real estate agents, allowing them to efficiently manage their property listings for clients on our platform. Our guidelines and independent evaluations from regional partners help to better understand the specifics of the real estate market in different countries.",
  },
  {
    title:
      "How do you ensure the accuracy and legitimacy of property information?",
    content:
      "Maintaining the accuracy and legitimacy of real estate information is paramount to us. We work closely with local lawyers and experts who can conduct comprehensive property checks and provide you with detailed legal and analytical reports, ensuring thorough due diligence.",
  },
  {
    title: "I'm a real estate developer. Can your platform benefit me?",
    content:
      "Certainly! Our platform offers you a great opportunity to showcase your real estate, share information about yourself and your construction experience. Real estate agents from different countries can access information about you and your properties to offer them to their clients.",
  },
];

const Faq = () => {
  return (
    <div id="faq" className="gridWrapper mb-[160px] sm:mb-[80px]">
      <div className="col-span-6">
        <Typography type="h1" className="text-center mb-[32px] sm:mb-[24px]">
          FAQ
        </Typography>
        <Accordeon items={accordionItems} />
      </div>
    </div>
  );
};

export default Faq;
