"use client";
import React, { useEffect, useState } from "react";
import { Typography } from "@/components/Typography";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import { Swiper as SwiperClass } from "swiper";
import "swiper/css/pagination";
import { EffectCoverflow, Mousewheel } from "swiper/modules";
import "./carouseStyles.css";
import Link from "next/link";
import Button from "../Button";

const staticWindow = () => {
  if (typeof window !== "undefined") {
    return window;
  }
  return {
    innerWidth: 1280,
  };
};

const SwiperControls = (instance: any) => {
  const swiper = instance.swiper as SwiperClass;
  const slides = Array.from({ length: swiper?.slides?.length }, (_, i) => i);

  useEffect(() => {
    swiper.update();
  }, [swiper.realIndex]);

  return (
    <div className="absolute transform left-1/2 -translate-x-1/2 bottom-0 flex gap-[5px]">
      {slides.map((slide: number) => (
        <div
          key={slide}
          className={`w-[12px] h-[12px] rounded-full mx-[4px] cursor-pointer ${
            slide === swiper.realIndex ? "bg-[#1B5C6B]" : "bg-[#E8EFEA]"
          }`}
          onClick={() => {
            swiper.slideTo(slide);
          }}
        ></div>
      ))}
    </div>
  );
};

const Slide = ({ src, text }: { src: string; text: string }) => (
  <div className="flex flex-col items-center text-center">
    <Image
      src="/assets/done.svg"
      alt="check"
      width={24}
      height={24}
      className="mb-[8px]"
    />
    <Typography
      type="subtitle2"
      className="carousel_text mb-[24px] w-full mx-auto text-black"
    >
      {text}
    </Typography>
    <Image
      className="carousel_mockup"
      src={src}
      alt="iphone mockup"
      width={320}
      height={654}
    />
  </div>
);

const Carousel = () => {
  const [swiper, setSwiper] = useState<SwiperClass | null>(null);
  const [_, setRealIndex] = useState<number | null>(null);
  const updateRealIndex = (swiper: SwiperClass) => {
    setRealIndex(swiper.realIndex);
  };

  return (
    <div className="carousel bg-primary-light pt-[68px] pb-[68px] w-fit xl:w-[100vw] sm:py-[60px] ">
      <div className="gridWrapper">
        <div className="relative col-span-6">
          <Typography
            type="h1"
            className="text-center mb-[32px] mx-auto max-w-[700px] sm:w-auto sm:font-[600] sm:mb-[24px]"
          >
            Create & share property selections in{" "}
            <span className="text-primary">2 clicks</span> – fast, personalized,
            and automatic!
          </Typography>
          <div className="relative pb-[44px] sm:pb-[28px] mb-[48px] sm:mb-[20px] -ml-[24px] w-[calc(100%+48px)]">
            <Swiper
              initialSlide={1}
              spaceBetween={100}
              effect={"coverflow"}
              grabCursor={true}
              centeredSlides={true}
              slidesPerView={staticWindow().innerWidth > 640 ? 3 : 1.7}
              coverflowEffect={{
                rotate: 0,
                stretch: 0,
                depth: 250,
                modifier: 1,
                slideShadows: false,
              }}
              mousewheel={{
                forceToAxis: true,
                sensitivity: 1,
                thresholdDelta: 25,
              }}
              direction="horizontal"
              pagination={true}
              modules={[EffectCoverflow, Mousewheel]}
              className="mySwiper"
              onSlideChange={updateRealIndex}
              onSwiper={(instance: any) => {
                if (swiper) return;
                setSwiper(instance);
              }}
            >
              <SwiperSlide>
                <Slide
                  src="/assets/iphone_mock0.png"
                  text="International real estate - Investment opportunities - Investment Allocation (presentation of your project). Local experts (market support, legal and bureaucratic support) - Increased international sales "
                />
              </SwiperSlide>
              <SwiperSlide>
                <Slide
                  src="/assets/iphone_mock.png"
                  text="Suppliers, products and services for real estate"
                />
              </SwiperSlide>
              <SwiperSlide>
                <Slide
                  src="/assets/iphone_mock1.png"
                  text="Life in coastal areas (coming soon) - Coastal jobs and staff (coming soon)"
                />
              </SwiperSlide>
            </Swiper>
            {swiper && <SwiperControls swiper={swiper} />}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Carousel;
