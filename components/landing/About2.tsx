import React from "react";
import Image from "next/image";
import { Typography } from "@/components/Typography";

const About2 = () => {
  return (
    <div
      id="about"
      className="gridWrapper mb-[100px] sm:flex sm:flex-wrap sm:mb-[60px]"
    >
      <Typography
        type="h1"
        className="hidden sm:block sm:text-center sm:w-full sm:font-[600]"
      >
        About platform
      </Typography>
      <Image
        width={498}
        height={360}
        src="/assets/about1.jpg"
        alt="img"
        className="col-span-3 rounded sm:col-span-6"
      />
      <div className="col-span-3 sm:col-span-6">
        <Typography type="h1" className="mb-[24px] sm:hidden">
          Tools to automate sales  <span className="text-primary"> and optimize resources </span>
        </Typography>
       
        <Typography type="body">
          <b>Accelerate international expansion and  sales</b>
        </Typography>
      </div>
    </div>
  );
};

export default About2;
