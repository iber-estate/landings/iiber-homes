import React from "react";
import { Typography } from "@/components/Typography";
import Button from "@/components/Button";
import Link from "next/link";

export const FinalCta = () => {
  return (
    <div className="flex flex-col items-center py-[80px] bg-blue w-fit xl:w-[100vw] sm:py-[40px]">
      <div className="w-[680px] text-center gridWrapper">
        <div className="col-span-6">
          <Typography
            type="h1"
            className="mb-[24px] w-[650px] mx-auto sm:w-[320px] sm:mx-auto sm:mb-[28px] sm:font-[600]"
          >
            Join an international ecosystem of real estate, investment and
            business solutions for coastal living.
          </Typography>
          <Link href="https://app.iber.homes/investment">
            <Button className="mx-auto"> Let's start! </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};
