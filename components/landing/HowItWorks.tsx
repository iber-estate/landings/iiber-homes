import React from "react";
import { Typography } from "@/components/Typography";
import Image from "next/image";

type BulletProps = {
  number: string;
  title: string;
};

const Bullet = ({ number, title }: BulletProps) => (
  <div className="flex items-center gap-[28px] sm:gap-[8px]">
    <Typography type="h2" className="sm:hidden min-w-[65px]">
      {number}
    </Typography>
    <Typography type="h1" className="hidden sm:block min-w-[32px]">
      {number}
    </Typography>
    <Typography type="subtitle" className="text-black sm:hidden">
      {title}
    </Typography>
    <Typography type="subtitle2" className="text-black hidden sm:block">
      {title}
    </Typography>
  </div>
);

const HowItWorks = () => {
  return (
    <div
      className="bg-primary-light mb-[160px] sm:mb-[80px] w-fit xl:w-[100vw]"
      id="hiw"
    >
      <div className="py-[120px] sm:py-[60px] gridWrapper sm:gap-[40px]">
        <div className="col-span-3 sm:col-span-6">
          <Typography type="h1" className="mb-[32px]">
            How it works
          </Typography>
          <div className="flex flex-col gap-[28px] sm:gap-[16px]">
            <Bullet number="01" title="Register" />
            <Bullet number="02" title="Get access to all properties" />
            <Bullet number="03" title="Make listings of properties you like" />
            <Bullet
              number="04"
              title="Share information with your clients in a convenient format"
            />
            <Bullet
              number="05"
              title="Contact us for assistance in processing your transaction"
            />
          </div>
        </div>
        <div className="col-span-3 grid grid-cols-3 sm:col-span-6">
          <Image
            src="/assets/howitworks1.jpg"
            alt="img"
            width={276}
            height={355}
            className="ratio-[276/355] col-span-2 rounded z-0 object-cover sm:object-center sm:aspect-[199/256] sm:w-[199px] sm:h-[256px]"
          />
          <Image
            src="/assets/howitworks2.jpg"
            alt="img"
            width={351}
            height={245}
            className="relative z-1 ratio-[351/245] col-start-2 col-span-2 rounded -mt-[55px] sm:aspect-[184/127] sm:w-[184px] sm:h-[127px] sm:ml-[25px] sm:mt-[-64px]"
          />
        </div>
      </div>
    </div>
  );
};

export default HowItWorks;
