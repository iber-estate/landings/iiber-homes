import React from "react";
import { Typography } from "@/components/Typography";

const Offer = () => {
  return (
    <div className="gridWrapper mb-[100px] sm:gap-[20px] sm:mb-[60px]">
      <Typography
        type="h2"
        className="
            col-span-6
            text-center
            text-primary-dark
            font-black
            mb-[16px]
            sm:mb-[0px]
        "
      >
        More than <span className="text-primary">40 000+</span> properties and
        investment project in <span className="text-primary">45+</span> costal
        regions.
        <p className="sm:text-primary text-[20px] font-[500] pt-[16px]">
          {" "}
          Featured Locations: Bali, Spain, Thailand, Georgia, Europe, South
          America & more
        </p>
      </Typography>
      <div className="h-[408px] rounded bg-lightblueone px-[28px] py-[40px] sm:py-[28px] col-span-2 sm:col-span-6 sm:h-auto">
        <Typography
          type="h4"
          className="w-[267px] mx-auto  mb-[16px] text-center sm:text-[20px] sm:leading-[24px] sm:font-[400]"
        >
          Local experts Market expertise, legal and administrative support
        </Typography>
        <Typography type="body" className="mb-[16px]">
          Only compliant and trasparent experts are acceptable to the ecosystem
        </Typography>
      </div>
      <div className="h-[408px] rounded bg-lightblueone px-[28px] py-[40px] sm:py-[28px] col-span-2 sm:col-span-6 sm:h-auto">
        <Typography
          type="h4"
          className="w-[267px] mx-auto  mb-[16px] text-center sm:text-[20px] sm:leading-[24px] sm:font-[400]"
        >
          Investment opportunities. Listing and tools for easy evaluation and
          presentation of business projects
        </Typography>
        <Typography type="body" className="mb-[16px]">
          Forecasting, analytics, calculations
        </Typography>
      </div>
      <div className="h-[408px] rounded bg-lightblueone px-[28px] py-[40px] sm:py-[28px] col-span-2 sm:col-span-6 sm:h-auto">
        <Typography
          type="h4"
          className="w-[267px] mx-auto mb-[16px] text-center sm:text-[20px] sm:leading-[24px] sm:font-[400]"
        >
          Real estate suppliers, equipment and service providers
        </Typography>
        <Typography type="body" className="mb-[16px] ">
          Facilities by the sea and in tourist destinations
        </Typography>
      </div>
    </div>
  );
};

export default Offer;
