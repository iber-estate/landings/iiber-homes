"use client";
import React from "react";
import { Typography } from "@/components/Typography";

interface ReviewCard {
  problems: string[];
  solutions: string[];
}

const reviewData: ReviewCard[] = [
  {
    problems: [
      "Lack of reliable partners – risk of fraud and unverified developers.",
      "Limited market knowledge – bureaucracy, legal complexities, and lack of local insights.",
    ],
    solutions: [
      "Verified partners and properties – safe transactions with trusted developers.",
      "Comprehensive market insights – detailed location data, legal clarity, and high-liquidity properties.",
    ],
  },
  {
    problems: [
      "Lengthy and complex sales processes.",
      "Inefficient operations.",
      "Difficulty in finding customers and negotiating.",
    ],
    solutions: [
      "Automation – accelerates customer prospecting and negotiations.",
      "Personalized account manager & flexible rates – support from startup to deal closure.",
    ],
  },
  {
    problems: [
      "Lack of analytics and market data.",
      "High costs of international scaling.",
    ],
    solutions: [
      "Cost-effective international expansion – reduced market entry expenses.",
      "Access to vetted foreign agents and investors – trusted global network.",
      "Analytics & support – market reports and growth strategies.",
    ],
  },
];

const Reviews = () => {
  const bgColors = ["bg-lightblueone", "bg-lightbluethree", "bg-lightbluetwo"];

  return (
    <div className="gridWrapper mb-[160px] sm:mb-[80px]">
      <div className="relative col-span-6">
        <Typography
          type="h1"
          className="text-center mb-[24px] sm:text-darkAquamarine sm:text-center sm:font-[600]"
        >
          Problems & Solutions
        </Typography>

        <div className="grid grid-cols-3 sm:grid-cols-1 gap-6 mt-8">
          {reviewData.map((card, index) => (
            <div
              key={index}
              className={`${bgColors[index]} rounded-lg p-6 shadow-md hover:shadow-lg transition-shadow border border-[#E5E9F2]`}
            >
              <div className="mb-6">
                <Typography
                  type="h4"
                  className="text-darkAquamarine mb-4 font-semibold"
                >
                  Problems
                </Typography>
                <ul className="list-disc pl-4 space-y-2">
                  {card.problems.map((problem, idx) => (
                    <li key={idx} className="text-[#4A5567] text-[16px]">
                      {problem}
                    </li>
                  ))}
                </ul>
              </div>

              <div>
                <Typography
                  type="h4"
                  className="text-darkAquamarine mb-4 font-semibold"
                >
                  Solutions
                </Typography>
                <ul className="list-disc pl-4 space-y-2">
                  {card.solutions.map((solution, idx) => (
                    <li key={idx} className="text-[#4A5567] text-[16px]">
                      {solution}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Reviews;
