import React from "react";
import { Typography } from "@/components/Typography";
import Image from "next/image";
import Button from "@/components/Button";
import Link from "next/link";

const Browse = () => {
  return (
    <div>
      <div className="flex flex-col items-center py-[80px] bg-blue w-fit xl:w-[100vw] sm:py-[40px] mb-[100px] sm:mb-[80px]">
        <div className="w-[680px] text-center gridWrapper">
          <div className="col-span-6">
            <Typography
              type="h1"
              className="mb-[24px] w-[650px] mx-auto sm:w-[320px] sm:mx-auto sm:mb-[28px] sm:font-[600]"
            >
              Join the international ecosystem Leave an application and we will
              contact you
            </Typography>
            <Link href="https://forms.gle/aPXnFbpSWfqzyyBG9  ">
              <Button className="mx-auto"> Submit a request </Button>
            </Link>
          </div>
        </div>
      </div>
      <div className="gridWrapper">
        {/* Container with responsive layout */}
        <div className="col-span-6 grid grid-cols-3 gap-[28px] mb-[100px] sm:mb-[60px] sm:grid-cols-1 sm:h-auto sm:gap-[28px]">
          {/* Left CTA Card */}
          <div className="px-[28px] py-[40px] bg-lightbluetwo rounded-[8px] sm:px-[20px] sm:py-[32px] sm:h-auto flex flex-col items-center text-center">
            <Typography
              type="body"
              className="mb-6 text-[#104551] font-[400] text-[20px] w-[220px] sm:w-full sm:mb-4 sm:text-[24px] sm:leading-[28px] sm:font-[400]"
            >
              Profitable international real estate Find buyers and investors for
              real estate developers Help developers to sell through our network
              of agents
            </Typography>
            <Link
              href="https://app.iber.homes/search"
              target="_blank"
              className="w-full"
            >
              <Button
                color="default"
                className="w-full sm:w-[180px] mx-auto font-[300] sm:font-[400]"
              >
                View properties
              </Button>
            </Link>
          </div>

          {/* Center Image */}
          <div className="relative sm:h-[180px] h-[360px]">
            <Image
              src="/assets/lux_villa.jpg"
              alt="Modern luxury house with pool"
              width={324}
              height={240}
              className="w-full h-full object-cover rounded-[8px]"
            />
          </div>

          {/* Right CTA Card */}
          <div className="px-[28px] py-[40px] bg-lightbluetwo rounded-[8px] sm:px-[20px] sm:py-[32px] sm:h-auto flex flex-col justify-start text-center">
            <Typography
              type="body"
              className="mb-6 text-[#104551] font-[400] text-[20px] w-[220px] sm:w-full sm:mb-4 sm:text-[24px] sm:leading-[28px] sm:font-[400]"
            >
              Buying or selling a business Finding buyers and investors for
              partners Help in finding businesses or projects for investment
            </Typography>
            <Link
              href="https://app.iber.homes/invest-search?page=1&ordering=-created_at"
              target="_blank"
              className="w-full"
            >
              <Button
                color="default"
                className="w-full sm:w-[180px] mx-auto  font-[300] sm:font-[400]"
              >
                View projects
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Browse;
