import React from "react";
import { Typography } from "@/components/Typography";

type AdvantageCardProps = {
  number: number;
  title: any;
  text: string;
  bgClass: string;
};

const AdvantageCard = ({
  number,
  title,
  text,
  bgClass,
}: AdvantageCardProps) => {
  return (
    <div
      className={`${bgClass} p-[38px] rounded col-span-2 sm:col-span-12 h-[520px] sm:h-[392px] sm:min-w-[250px]`}
    >
      <Typography type="h1" className="mb-[60px] sm:mb-[40px]">
        0{number}
      </Typography>
      <Typography type="h4" className="mb-[20px] text-black sm:hidden">
        {title}
      </Typography>
      <Typography
        type="h2"
        className="mb-[20px] text-black hidden sm:block sm:font-[600]"
      >
        {title}
      </Typography>
      <Typography type="body">{text}</Typography>
    </div>
  );
};

const Advantages = () => {
  return (
    <div id={"advantages"} className="mb-[160px] sm:mb-[80px]">
      <Typography
        type="h1"
        className="w-full text-center mb-[24px] sm:font-[600]"
      >
        Publication rates
      </Typography>
      <div className="gridWrapper sm:grid-cols-1 sm:overflow-scroll sm:mb-[70px] sm:pb-[10px] sm:flex">
        <AdvantageCard
          number={1}
          title="Projects from Developers"
          bgClass="bg-lightblueone"
          text={`
            Single Property Listing – $100/year

5–10 Property Package – $85 per property/year

10+ Property Package – $70 per property/year

🔹 Includes a property card with a detailed description and up to three floor plans.`}
        />
        <AdvantageCard
          number={2}
          title="Business for Sale"
          bgClass="bg-lightbluethree"
          text="Business Listing – $99/year

Suitable for hotels, restaurants, cafes, retail spaces, and other commercial properties."
        />
        <AdvantageCard
          number={3}
          title="Exclusive Promotion for Developers"
          bgClass="bg-lightbluetwo"
          text="Project Packaging and Promotion 
Price: $500/month

 Full-service support 

from launch to closing the deal.

Price – starting from $2,000/month."
        />
        <AdvantageCard
          number={4}
          title="Investment Projects"
          bgClass="bg-lightbluethree"
          text="Project Listing – $105/year

Investments in manufacturing, business ideas, startups and other projects.

"
        />
        {/* <AdvantageCard
          number={5}
          title="Up-to-Date Market Information"
          bgClass="bg-lightbluetwo"
          text="We provide relevant data on real estate and local markets to help you make confident decisions.
Our partners and independent expertise will assist you in analyzing key aspects of regional markets and assessing potential opportunities."
        />
        <AdvantageCard
          number={6}
          title="Convenient Tools for Work"
          bgClass="bg-lightblueone"
          text="You can easily select suitable properties, save them, and create collections to share with friends or clients.
This allows real estate agents to quickly find suitable offers and interact with clients efficiently, demonstrating a high level of professionalism."
        /> */}
      </div>
    </div>
  );
};

export default Advantages;
