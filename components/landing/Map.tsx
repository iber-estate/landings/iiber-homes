import React from "react";
import Icon from "@/components/landing/MapSVG";
import { Typography } from "@/components/Typography";
import Image from "next/image";

const Map = () => {
  return (
    <div className="mb-[68px] gridWrapper sm:hidden">
      <div className="w-full flex justify-center col-span-6">
        <div className="mb-10 ">
          <Image src="/assets/map1.svg" alt="map" width={1376} height={575} />
        </div>
      </div>
    </div>
  );
};

export default Map;
