import React from "react";
import { Typography } from "@/components/Typography";
import Image from "next/image";

const Bullet = ({ title, subtitle }: { title: string; subtitle: string }) => (
  <div className="sm:text-center">
    <Typography type="hero" className="mb-[0.75rem]" tag="h2">
      {title}
    </Typography>
    <Typography type="h3" className="text-primary-light sm:hidden">
      {subtitle}
    </Typography>
    <Typography
      type="subtitle2"
      className="text-primary-light hidden sm:block sm:text-primary"
    >
      {subtitle}
    </Typography>
  </div>
);

const About = () => {
  return (
    <div className={`gridWrapper mb-[68px] sm:mb-[80px]`} id="about">
      <div className="col-span-3 sm:col-span-6">
        <Typography type="h1" className="mb-[2rem] sm:mb-[24px]">
          About platform
        </Typography>
        <Image
          src="/assets/about.jpeg"
          width={498}
          height={380}
          alt="img"
          className="w-full aspect-[498/380] rounded mb-[24px] object-center object-cover sm:aspect-[312/180] sm:mb-[12px] sm:object-cover sm:object-top"
        />
        <Typography type="subtitle2" className="leading-[28px]">
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          Looking for <b>regional partners</b> or <b>independent experts</b>?
          Interested in new ideas for <b>investments</b> or <b>business</b> in
          your chosen location? We have everything you need. It's simple and
          convenient!
        </Typography>
      </div>
      <div className="col-span-3 sm:col-span-6 sm:flex sm:flex-col-reverse">
        <Typography type="h2" className="hidden sm:block text-center">
          Turkey, Thailand, Bulgaria, United Arab Emirates, Indonesia
        </Typography>
        <div className="flex flex-wrap gap-y-[4.3rem] gap-x-[4.5rem] pt-[92px] mb-[122px] sm:gap-x-0 sm:justify-between sm:pt-[80px] sm:mb-[1.5rem]">
          <Typography type="subtitle2" className="leading-[28px]">
            <b>
              Complexes, villas, and a wide selection of properties from
              developers by the sea or ocean
            </b>{" "}
            in various countries with detailed descriptions of location,
            neighborhood features, and infrastructure. Explore detailed property
            descriptions to get complete information about each listing.
          </Typography>
        </div>
        <div className="sm:flex sm:gap-[24px]">
          <Image
            src="/assets/about1.jpg"
            width={500}
            height={405}
            alt="img"
            className="w-full aspect-[498/261] rounded mb-[24px] sm:aspect-[144/208] sm:w-1/2 sm:object-cover sm:object-center sm:mb-0"
          />
        </div>
      </div>
    </div>
  );
};

export default About;
