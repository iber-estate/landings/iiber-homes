import React from "react";
import Button from "@/components/Button";
import Image from "next/image";
import { Typography } from "@/components/Typography";
import Link from "next/link";

const HeroSection = () => {
  return (
    <div className="gridWrapper">
      <div
        className={`sm:p-0 col-span-6 overflow-hidden rounded relative flex py-[113px] px-[56px] mb-[100px] mt-[24px] sm:mt-0 sm:mb-[0px] sm:flex-wrap`}
      >
        <div className="z-20 bg-black left-0 top-0 opacity-40 absolute h-full w-full sm:hidden"></div>
        <div>
          <Image
            className="z-10 absolute left-0 top-0 w-full sm:rounded aspect-[498/380] sm:static sm:col-span-6"
            src="/assets/hero.jpg"
            alt="hero image"
            width={498}
            height={380}
          />
          <Link href="https://calendly.com/iberhomes" target="_blank">
            <Button
              color="white"
              className="hidden sm:block -translate-y-[100%] -mt-[20px] mx-auto z-1 relative"
            >
              Book a call
            </Button>
          </Link>
        </div>

        <div className="order-first w-[900px] sm:w-auto mx-auto relative z-30 flex flex-col items-center col-span-6 text-center">
          <Typography
            type="h2"
            className={`
            text-[#F9F9F8]
            font-black
            mb-[16px]
            sm:mb-[12px]
            sm:text-darkAquamarine
        `}
          >
            Fast access to thousands of international developers, buyers,
            investors,{" "}
            <span className="sm:text-primary">
              projects and suppliers from over 45 costal countries.
            </span>
          </Typography>
          <p
            className={`max-w-[520px] mb-[24px]
          font-onest font-[300] text-[20px] text-[#F9F9F8] sm:text-[16px] sm:leading-[20px] sm:mb-0 sm:text-black
          `}
          >
            International Ecosystem - Community and Opportunities for mutuall
            beneficial collaboration.
          </p>
          <Link href="https://calendly.com/iberhomes" target="_blank">
            <Button color="light" className="sm:hidden block">
              Book a call
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default HeroSection;
