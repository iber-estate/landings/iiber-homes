import React from "react";
import { twMerge } from "tailwind-merge";

type TextProps = {
  type:
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "text"
    | "paragraph"
    | "info"
    | "hero"
    | "subtitle"
    | "h6"
    | "subtitle2"
    | "caption"
    | "body";

  children: React.ReactNode;
  className?: string;
  tag?: string;
};

export const Typography: React.FC<TextProps> = ({
  type,
  children,
  className,
  tag,
}) => {
  let resultParams = { tag: "span", className: "" };
  switch (type) {
    case "hero":
      resultParams = {
        className:
          "font-satoshi text-[3.25rem] leading-[3.5rem] font-black sm:text-[24px] sm:leading-[28px] sm:font-[600]",
        tag: "h1",
      };
      break;
    case "h1":
      resultParams = {
        className:
          "font-satoshi text-[3rem] leading-[3.75rem] font-black sm:text-[1.5rem] sm:leading-[1.75rem]",
        tag: "h2",
      };
      break;
    case "h2":
      resultParams = {
        className:
          "font-satoshi text-[2.75rem] leading-[3rem] font-black sm:text-[1rem] sm:leading-[1.25rem] sm:font-[500]",
        tag: "h3",
      };
      break;
    case "h3":
      resultParams = {
        className: "font-satoshi text-[2.5rem] leading-[3rem] font-semibold",
        tag: "h4",
      };
      break;
    case "h4":
      resultParams = {
        className: "text-[1.75rem] leading-[2rem] font-medium",
        tag: "h5",
      };
      break;
    case "paragraph":
      resultParams = {
        className: "text-base text-[#4E4B66] leading-8",
        tag: "p",
      };
      break;
    case "subtitle":
      resultParams = {
        className:
          "text-[1.75rem] leading-[2rem] font-[200] sm:text-[16px] sm:leading-[20px]",
        tag: "p",
      };
      break;
    case "h6":
      resultParams = {
        className: "text-[1.25rem] leading-[1.75rem] font-[500]",
        tag: "h6",
      };
      break;
    case "subtitle2":
      resultParams = {
        className:
          "text-[1.25rem] leading-[1.5rem] font-[200] sm:text-[0.875rem] sm:leading-[1rem] sm:font-[300]",
        tag: "p",
      };
      break;
    case "caption":
      resultParams = {
        className: "text-[0.825rem] leading-[1.25rem] font-regular",
        tag: "p",
      };
      break;
    case "body":
      resultParams = {
        className:
          "text-black text-[1rem] leading-[1.25rem] font-[200] sm:text-[0.8125rem] sm:leading-[1rem]",
        tag: "p",
      };
      break;
    case "text":
      resultParams = {
        className: "text-base text-[#4E4B66] leading-8",
        tag: "span",
      };
      break;
    case "info":
      resultParams = {
        className: "text-base text-[#6E7191]",
        tag: "small",
      };
      break;
  }

  resultParams.className = twMerge(
    "text-darkAquamarine",
    resultParams.className,
    className,
  );
  return React.createElement(
    tag || resultParams.tag,
    { className: resultParams.className },
    children,
  );
};
