"use client";
import React, { createContext, useState } from "react";

type StoreType =
  | {
      count: number;
      setStore: any;
    }
  | any;

export const StoreContext = createContext<StoreType>(null);
export const useStore = () => useState({ count: 1 });

const StoreProvider = ({ children }: { children: React.ReactNode }) => {
  const [store, setStore] = useStore();
  return (
    <StoreContext.Provider value={[store, setStore]}>
      {children}
    </StoreContext.Provider>
  );
};

export default StoreProvider;
