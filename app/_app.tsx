"use client";
import React from "react";
import { Inter } from "next/font/google";
import "mapbox-gl/dist/mapbox-gl.css";
import "react-loading-skeleton/dist/skeleton.css";
import Head from "next/head";
// If loading a variable font, you don't need to specify the font weight

export default function MyApp({ Component, pageProps }: any) {
  return (
    <div>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"
        />
      </Head>
      <main>
        <Component {...pageProps} />
      </main>
    </div>
  );
}
