import React from "react";
import HeroSection from "@/components/landing/HeroSection";
import About from "@/components/landing/About";
import Map from "@/components/landing/Map";
import Browse from "@/components/landing/Browse";
import Advantages from "@/components/landing/Advantages";
import { Typography } from "@/components/Typography";
import HowItWorks from "@/components/landing/HowItWorks";
import Reviews from "@/components/landing/Reviews";
import Faq from "@/components/landing/FAQ";
import { FinalCta } from "@/components/landing/FinalCTA";
import Carousel from "@/components/landing/Carousel";
import { Metadata } from "next";
import Offer from "@/components/landing/Offer";
import About2 from "@/components/landing/About2";

export const metadata: Metadata = {
  title:
    "Iber | Real estate from developers and business by the sea. Apartments, villas, commercial properties, property for renovation. Projects and ideas for investment.",
  description:
    "Commercial properties and renovation projects. Investment projects.Regional partners. Local experts.",
  icons: {
    icon: "/assets/favicon.ico",
  },
  robots: {
    index: true,
    follow: true,
    nocache: false,
    googleBot: {
      index: true,
      follow: true,
    },
  },
};

const Page = () => {
  return (
    <div className="bg-beige">
      <HeroSection />
      <Offer />
      {/*<About />*/}
      <About2 />
      {/*<Map />*/}
      <Carousel />
      <Browse />
      <Advantages />
      {/*<div className="gridWrapper">*/}
      {/*  <Typography*/}
      {/*    type="h3"*/}
      {/*    className="text-center mb-[160px] col-span-6 sm:hidden"*/}
      {/*  >*/}
      {/*    Find <span className="text-primary">Your Dream Home</span> on the*/}
      {/*    Ultimate <br /> Real Estate Marketplace*/}
      {/*  </Typography>*/}
      {/*  <Typography*/}
      {/*    type="h2"*/}
      {/*    className="text-center sm:max-w-[312px] sm:block hidden sm:mx-auto col-span-6 mb-[80px]"*/}
      {/*  >*/}
      {/*    Find <span className="text-primary">Your Dream Home</span>{" "}*/}
      {/*    on the Ultimate Real Estate Marketplace*/}
      {/*  </Typography>*/}
      {/*</div>*/}
      {/*<HowItWorks />*/}
      <Reviews />
      <Faq />
      <FinalCta />
    </div>
  );
};

export default Page;
