import "./globals.css";
import { Header } from "@/components/Header";
import cn from "classnames";
import { Footer } from "@/components/Footer";
import { satoshi, onest } from "@/fonts";
import "swiper/css";
import StoreProvider from "@/StoreContext";
import React from "react";
import { GoogleTagManager, GoogleAnalytics } from "@next/third-parties/google";
import Script from "next/script";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <Script
          async
          id="gtaginit"
          src="https://www.googletagmanager.com/gtag/js?id=G-G41JF4WF2N"
        ></Script>
        <Script
          id="gtagscript"
          dangerouslySetInnerHTML={{
            __html: `window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-G41JF4WF2N');`,
          }}
        ></Script>
        <meta
          name="google-site-verification"
          content="VQVd0dNe8KZXhlRezVfllV_D832V3wfQsWdCAMs2fso"
        />
      </head>
      <body className={cn(onest.className, satoshi.variable)}>
        <StoreProvider>
          <Header />
          <div>{children}</div>
          <Footer />
        </StoreProvider>
      </body>
      {/*<GoogleTagManager gtmId="G-G41JF4WF2N" />*/}
      {/*<GoogleAnalytics gaId="G-G41JF4WF2N" />*/}
    </html>
  );
}
