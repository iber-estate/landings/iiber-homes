import React from "react";
import { Typography } from "@/components/Typography";
import "./styles.css";
import Terms from "./terms";

const Page = () => {
  return (
    <div className="gridWrapper text-black">
      <div className="col-span-6">
        <Terms />
      </div>
    </div>
  );
};

export default Page;
