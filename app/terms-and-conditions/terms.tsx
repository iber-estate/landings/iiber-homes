import React from "react";
import "./styles.css";

const Terms = () => {
  return (
    <>
      <div id="sidebar">
        <div id="outline" />
      </div>
      <div id="page-container">
        <div className="pf w0 h0" data-page-no="1" id="pf1">
          <div className="pc pc1 w0 h0">
            <div className="t m0 x1 h2 y1 ff1 fs0 fc0 sc0 ls0 ws0">
              TERMS AND CONDITIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y2 ff2 fs1 fc1 sc0 ls2 ws0">
              Last updated November 15, 2023
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y3 ff3 fs2 fc0 sc0 ls3 ws0">
              AGREEMENT TO OUR LEGAL TERMS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y4 ff4 fs1 fc1 sc0 ls4 ws0">
              We are Iber Sma
              <span className="_ _0" />
              rt System, do
              <span className="_ _0" />
              ing business as
              <span className="_ _0" /> Iber (&quot;
              <span className="ff2 ls5">Company</span>
              <span className="ls6">
                ,&quot; &quot;<span className="ff2 ls5">we</span>
                ,&quot; &quot;<span className="ff2 ls2">us</span>
                ,&quot; &quot;<span className="ff2 ls2">our</span>
                <span className="ls7">
                  "), a company registered in Indonesia at Jl. Gn. Krakatau
                  No.25,{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y5 ff4 fs1 fc1 sc0 ls8 ws0">
              Pemecutan Klod, Kec. Denpasar Bar.
              <span className="_ _0" />, Kota Denpasar, Bali 80112,{" "}
              <span className="_ _0" />
              Indonesia, Denpasar, Bali 801
              <span className="_ _0" />
              12.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y6 ff4 fs1 fc1 sc0 ls4 ws0">
              We operate the <span className="_ _0" />
              website{" "}
              <span className="fc2 ls9">
                https://iberr
                <span className="_ _0" />
                est.com{" "}
                <span className="fc1 lsa">
                  (the "<span className="ff2 ls8">Site</span>
                  <span className="ls7">
                    "), as well as any other related products and services that
                    refer or link to these legal terms{" "}
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y7 ff4 fs1 fc1 sc0 lsa ws0">
              (the "<span className="ff2 ls2">Legal Terms</span>
              <span className="ls7">
                ") (collectively, the "<span className="ff2 ls8">Services</span>
                ").
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y8 ff4 fs1 fc1 sc0 ls6 ws0">
              Iber provides a vast coasta
              <span className="_ _1" />
              l real estate database, helping
              <span className="_ _1" /> real estate agents quickly fin
              <span className="_ _1" />
              d new properties, expandin
              <span className="_ _1" />g their po
              <span className="lsa">rtfolio and revenue </span>
            </div>
            <div className="t m0 x1 h5 y9 ff4 fs1 fc1 sc0 ls9 ws0">
              potential by offering them to clients
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ya ff4 fs1 fc1 sc0 ls8 ws0">
              You can contact us by email a
              <span className="_ _0" />
              t info@iberrest.com or by mail
              <span className="_ _0" /> to Jl. Gn. Krakatau No.25,{" "}
              <span className="_ _0" />
              Pemecutan Klod, Kec. Denpasar Bar.
              <span className="_ _0" />, Ko
              <span className="ls6">ta Denpasar, </span>
            </div>
            <div className="t m0 x1 h5 yb ff4 fs1 fc1 sc0 ls8 ws0">
              Bali 80112, Indonesia, Denpasa
              <span className="_ _0" />
              r, Bali 80112, Indonesia.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yc ff4 fs1 fc1 sc0 ls2 ws0">
              These Legal Terms constitute a legally bi
              <span className="_ _0" />
              nding agreement made between you, whether personally or{" "}
              <span className="_ _0" />
              on behalf of an entity ("
              <span className="ff2 ls9">you</span>
              <span className="ls7">"), and Iber </span>
            </div>
            <div className="t m0 x1 h5 yd ff4 fs1 fc1 sc0 ls8 ws0">
              Smart System, concerning your a
              <span className="_ _0" />
              ccess to and use of the Serv
              <span className="_ _0" />
              ices. You agree that by acce
              <span className="_ _0" />
              ssing the Services, you have <span className="_ _0" />
              read, und
              <span className="ls9">erstood, and </span>
            </div>
            <div className="t m0 x1 h5 ye ff4 fs1 fc1 sc0 ls9 ws0">
              agreed to be bound by all of these Legal Terms. I
              <span className="_ _0" />F YOU DO NOT AGREE WITH ALL OF THESE
              LEGAL TERMS, THEN Y<span className="ls5">OU ARE </span>
            </div>
            <div className="t m0 x1 h5 yf ff4 fs1 fc1 sc0 ls8 ws0">
              EXPRESSLY PROHIBITED FROM USING THE SERVICES AND YOU MUST
              DISCONTINUE USE IMMEDIATELY.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y10 ff4 fs1 fc1 sc0 ls8 ws0">
              Supplemental terms and conditi
              <span className="_ _0" />
              ons or documents that may be po
              <span className="_ _0" />
              sted on the Services from ti
              <span className="_ _0" />
              me to time are hereby express
              <span className="_ _0" />
              ly inco
              <span className="lsa">rporated herein </span>
            </div>
            <div className="t m0 x1 h5 y11 ff4 fs1 fc1 sc0 ls9 ws0">
              by reference. We reserve the right, in our sol
              <span className="_ _0" />
              e discretion, to make changes or modifications to t
              <span className="_ _0" />
              hese Legal Terms f
              <span className="lsa">rom time to time. We will alert </span>
            </div>
            <div className="t m0 x1 h5 y12 ff4 fs1 fc1 sc0 lsb ws0">
              you about any changes by updating the "Last updated" date{" "}
              <span className="_ _0" />
              of these Legal Terms, and you waive any right to recei
              <span className="_ _0" />
              ve specific n
              <span className="_ _1" />
              <span className="ls9">otice of each such </span>
            </div>
            <div className="t m0 x1 h5 y13 ff4 fs1 fc1 sc0 lsb ws0">
              change. It is your responsibility to peri
              <span className="_ _0" />
              odically review these Legal Terms to st
              <span className="ls9">
                ay informed of updates. You will be subject to, and{" "}
                <span className="_ _0" />
                will be deemed to{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y14 ff4 fs1 fc1 sc0 ls9 ws0">
              have been made aware of and to have accepted, the changes in any{" "}
              <span className="_ _0" />
              revised Legal Terms by your continued use of the Services af
              <span className="_ _0" />
              <span className="ls6">ter the date </span>
            </div>
            <div className="t m0 x1 h5 y15 ff4 fs1 fc1 sc0 lsb ws0">
              such revised Legal Terms are posted.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y16 ff4 fs1 fc1 sc0 ls2 ws0">
              The Services are intended for users who ar
              <span className="_ _0" />e at least 18 years old. Persons under{" "}
              <span className="_ _0" />
              the age of 18 are not permitted to use or
              <span className="_ _0" /> re
              <span className="ls9">gister for the </span>
            </div>
            <div className="t m0 x1 h5 y17 ff4 fs1 fc1 sc0 ls8 ws0">
              Services.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y18 ff4 fs1 fc1 sc0 ls4 ws0">
              We recommend that
              <span className="_ _0" /> you print a
              <span className="_ _0" /> copy of thes
              <span className="_ _0" />
              e Legal Terms f
              <span className="_ _0" />
              or your recor
              <span className="_ _0" />
              ds.
              <span className="ls1"> </span>
            </div>
            <br />
            <br />
            <div className="t m0 x1 h4 y19 ff3 fs2 fc0 sc0 lsc ws0">
              TABLE OF CONTENTS
              <span className="ls1"> </span>
            </div>
            <br />
            <div className="t m0 x1 h5 y1a ff4 fs1 fc2 sc0 ls9 ws0">
              1. OUR SERVICES
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y1b ff4 fs1 fc2 sc0 ls9 ws0">
              2. INTELLECTUAL PROPERTY RIGHTS 3. USER REPRESENTATIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y1c ff4 fs1 fc2 sc0 ls9 ws0">
              4. USER REGISTRATION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y1d ff4 fs1 fc2 sc0 ls9 ws0">
              5. PURCHASES AND PAYMENT
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y1e ff4 fs1 fc2 sc0 ls9 ws0">
              6. POLICY
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y1f ff4 fs1 fc2 sc0 ls9 ws0">
              7. PROHIBITED ACTIVITIES
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y20 ff4 fs1 fc2 sc0 ls9 ws0">
              8. USER GENERATED CONTRIBUTIONS 9. CONTRIBUTION LICENSE
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y21 ff4 fs1 fc2 sc0 ls9 ws0">
              10. GUIDELINES FOR REVIEWS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y22 ff4 fs1 fc2 sc0 ls9 ws0">
              11. SOCIAL MEDIA
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y23 ff4 fs1 fc2 sc0 ls9 ws0">
              12. THIRD
              <span className="ls1">
                -<span className="ls8">PARTY WEBSITES AND CONTENT</span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y24 ff4 fs1 fc2 sc0 ls9 ws0">
              13. SERVICES MANAGEMENT
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y25 ff4 fs1 fc2 sc0 ls9 ws0">
              14. PRIVACY POLICY
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y26 ff4 fs1 fc2 sc0 ls9 ws0">
              15. TERM AND TERMINATION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y27 ff4 fs1 fc2 sc0 ls9 ws0">
              16. MODIFICATIONS AND INTERRUPTIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y28 ff4 fs1 fc2 sc0 ls9 ws0">
              17. GOVERNING LAW
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y29 ff4 fs1 fc2 sc0 ls9 ws0">
              18. DISPUTE RESOLUTION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y2a ff4 fs1 fc2 sc0 ls9 ws0">
              19. CORRECTIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y2b ff4 fs1 fc2 sc0 ls9 ws0">
              20. DISCLAIMER
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y2c ff4 fs1 fc2 sc0 ls9 ws0">
              21. LIMITATIONS OF LIABILI
              <span className="ls2">
                TY
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y2d ff4 fs1 fc2 sc0 ls9 ws0">
              22. INDEMNIFICATION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y2e ff4 fs1 fc2 sc0 ls9 ws0">
              23. USER DATA
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y2f ff4 fs1 fc2 sc0 ls9 ws0">
              24. ELECTRONIC COMMUNICATIONS, TRANSACTIONS, AND SIGNATURES 25.
              CALIFORNIA USERS AND RESIDENTS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y30 ff4 fs1 fc2 sc0 ls9 ws0">
              26. MISCELLANEOUS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y31 ff4 fs1 fc2 sc0 ls9 ws0">
              27. CONTACT US
              <span className="ls1"> </span>
            </div>
            <br />
            <br />
            <div className="t m0 x1 h4 y32 ff3 fs2 fc0 sc0 lsd ws0">
              1. OUR SERVICES
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y33 ff4 fs1 fc1 sc0 ls2 ws0">
              The information provided when using the Servic
              <span className="_ _0" />
              es is not intended for distributi
              <span className="_ _0" />
              on to or use by any person or entity <span className="_ _0" />
              in any ju
              <span className="lsa">risdiction or country </span>
            </div>
            <div className="t m0 x1 h5 y34 ff4 fs1 fc1 sc0 ls5 ws0">
              where such distributi
              <span className="_ _0" />
              on or use would be cont
              <span className="_ _0" />
              rary to law or regul
              <span className="_ _0" />
              ation or which would s
              <span className="_ _0" />
              ubject us to any regi
              <span className="_ _0" />
              st
              <span className="lsa">ration requirement within such </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="2" id="pf2">
          <div className="pc pc2 w0 h0">
            <div className="t m0 x1 h5 y35 ff4 fs1 fc1 sc0 ls6 ws0">
              jurisdiction or country. Accor
              <span className="_ _1" />
              dingly, those persons who
              <span className="_ _1" /> choose to access the Serv
              <span className="_ _1" />
              ices from other locations do
              <span className="_ _1" /> so on their own
              <span className="ls1"> </span>
              initiative and are{" "}
            </div>
            <div className="t m0 x1 h5 y36 ff4 fs1 fc1 sc0 lsb ws0">
              solely responsible for compliance with local laws,
              <span className="_ _0" /> if and to the extent local laws are appl
              <span className="_ _0" />
              icable.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y37 ff4 fs1 fc1 sc0 ls2 ws0">
              The Services are not tailored to comply
              <span className="_ _0" /> with industry
              <span className="ls1">
                -
                <span className="lsb">
                  specific regulations (Health Insurance Port
                  <span className="_ _0" />
                  ability and Accountability Act (HIPAA), Federal{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y38 ff4 fs1 fc1 sc0 ls6 ws0">
              Information Security Manag
              <span className="_ _1" />
              ement Act (FISMA), etc.), so if yo
              <span className="_ _1" />
              ur interactions would be su
              <span className="_ _1" />
              bjected to such laws, yo
              <span className="ls9">u may not use the Services. You </span>
            </div>
            <div className="t m0 x1 h5 y39 ff4 fs1 fc1 sc0 lse ws0">
              may not use the <span className="_ _0" />
              Services in a way
              <span className="_ _0" /> that would vio
              <span className="_ _0" />
              late the Gramm
              <span className="ls1">
                -<span className="ls9">Leach</span>-
                <span className="ls8">
                  Bliley Act (GLBA)
                  <span className="_ _0" />.<span className="ls1"> </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h4 y3a ff3 fs2 fc0 sc0 lsd ws0">
              2. INTELLECTUAL PROPERTY RIGHTS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h6 y3b ff5 fs3 fc0 sc0 lsf ws0">
              Our int
              <span className="_ _1" />
              ellect
              <span className="_ _1" />
              ual pr
              <span className="_ _1" />
              operty
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y3c ff4 fs1 fc1 sc0 ls4 ws0">
              We are the owne
              <span className="_ _0" />
              r or the lic
              <span className="_ _0" />
              ensee of all <span className="_ _0" />
              intellectual
              <span className="_ _0" /> property rig
              <span className="_ _0" />
              hts in our Se
              <span className="_ _0" />
              rvices, inclu
              <span className="_ _0" />
              ding all sour
              <span className="_ _0" />
              ce code, datab
              <span className="_ _0" />
              ases, f
              <span className="ls9">
                unctionality, soft
                <span className="_ _0" />
                ware,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y3d ff4 fs1 fc1 sc0 ls5 ws0">
              website designs, audio,
              <span className="_ _0" /> video, text, phot
              <span className="_ _0" />
              ographs, and graphics i
              <span className="_ _0" />
              n the Services (coll
              <span className="_ _0" />
              ectively, the "Conten
              <span className="_ _0" />
              t"
              <span className="lsa">
                ), as well as the trademarks, service{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y3e ff4 fs1 fc1 sc0 lse ws0">
              marks, and logos <span className="_ _0" />
              contained there
              <span className="_ _0" />
              in (the "Marks")
              <span className="_ _0" />.<span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y3f ff4 fs1 fc1 sc0 ls5 ws0">
              Our Content and Marks are
              <span className="_ _0" /> protected by copyright
              <span className="_ _0" /> and trademark laws (an
              <span className="_ _0" />
              d various other intel
              <span className="_ _0" />
              lectual property ri
              <span className="_ _0" />
              ghts and unfai
              <span className="lsa">r competition laws) </span>
            </div>
            <div className="t m0 x1 h5 y40 ff4 fs1 fc1 sc0 ls9 ws0">
              and treaties in the United States and around t
              <span className="_ _0" />
              he world.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y41 ff4 fs1 fc1 sc0 ls2 ws0">
              The Content and Marks are provided in or thr
              <span className="_ _0" />
              ough the Services "AS IS" for your per
              <span className="_ _0" />
              sonal, non
              <span className="ls1">
                -
                <span className="lsb">
                  commercial use or internal business purpose only.
                </span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h6 y42 ff5 fs3 fc0 sc0 ls10 ws0">
              Your use of <span className="_ _1" />
              our Servi
              <span className="_ _1" />
              ces
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y43 ff4 fs1 fc1 sc0 ls8 ws0">
              Subject to your compliance wit
              <span className="_ _0" />
              h these Legal Terms, includi
              <span className="_ _0" />
              ng the "<span className="fc2">PROHIBITED ACTIVITIES</span>
              <span className="ls7">
                " section below, we grant you a non
                <span className="ls1">
                  -<span className="ls9">exclusive, non</span>-
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y44 ff4 fs1 fc1 sc0 ls6 ws0">
              transferable, revocable licen
              <span className="_ _1" />
              se to:
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x2 h5 y45 ff4 fs1 fc1 sc0 ls9 ws0">
              1.
              <span className="ls1">
                {" "}
                <span className="_ _2"> </span>
              </span>
              access the Services; and download or print a copy of any{" "}
              <span className="_ _0" />
              portion of the Content to which you have properly{" "}
              <span className="_ _0" />
              gained access.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x2 h5 y46 ff4 fs1 fc1 sc0 ls9 ws0">
              2.
              <span className="ls1">
                {" "}
                <span className="_ _2"> </span>
                <span className="lsb">solely for your personal, non</span>-
                <span className="lsb">
                  commercial use or internal business purpose.
                </span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y47 ff4 fs1 fc1 sc0 ls8 ws0">
              Except as set out in this <span className="_ _0" />
              section or elsewhere in our
              <span className="_ _0" /> Legal Terms, no part of th
              <span className="_ _0" />e Services and no Content or{" "}
              <span className="_ _0" />
              Marks may be cop
              <span className="ls6">ied, reproduced, </span>
            </div>
            <div className="t m0 x1 h5 y15 ff4 fs1 fc1 sc0 ls9 ws0">
              aggregated, republished, uploaded, posted, publicly{" "}
              <span className="_ _0" />
              displayed, encoded, translated, transmitted, dist
              <span className="_ _0" />
              ributed, sold
              <span className="ls6">, licensed, or otherwise exploited </span>
            </div>
            <div className="t m0 x1 h5 y48 ff4 fs1 fc1 sc0 ls6 ws0">
              for any commercial purpos
              <span className="_ _1" />
              e whatsoever, without our e
              <span className="_ _1" />
              xpress prior written permis
              <span className="_ _1" />
              sion.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y49 ff4 fs1 fc1 sc0 ls6 ws0">
              If you wish to make any use
              <span className="_ _1" /> of the Services, Content, or M
              <span className="_ _1" />
              arks other than as set out in th
              <span className="_ _1" />
              is section or elsewhere in ou
              <span className="_ _1" />r Legal Terms, please{" "}
            </div>
            <div className="t m0 x1 h5 y4a ff4 fs1 fc1 sc0 ls9 ws0">
              address your request to: info@iberrest.com. If we ever
              <span className="_ _0" /> grant you the permission to post,
              reproduce, or <span className="_ _0" />
              publicly display any part of our Services or
              <span className="_ _0" />{" "}
            </div>
            <div className="t m0 x1 h5 y4b ff4 fs1 fc1 sc0 ls5 ws0">
              Content, you must ident
              <span className="_ _0" />
              ify us as the owners <span className="_ _0" />
              or licensors of the
              <span className="_ _0" /> Services, Content, o
              <span className="_ _0" />r Marks and ensure that{" "}
              <span className="_ _0" />
              any copyright or{" "}
              <span className="ls9">
                propri
                <span className="_ _0" />
                etary notice{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y4c ff4 fs1 fc1 sc0 ls9 ws0">
              appears or is visible on posting, reproducing,{" "}
              <span className="_ _0" />
              or displaying our Content.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y4d ff4 fs1 fc1 sc0 ls4 ws0">
              We reserve all
              <span className="_ _0" /> rights not <span className="_ _0" />
              expressly grant
              <span className="_ _0" />
              ed to you in <span className="_ _0" />
              and to the Se
              <span className="_ _0" />
              rvices, Conten
              <span className="_ _0" />
              t, and Marks.
              <span className="_ _0" />
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y4e ff4 fs1 fc1 sc0 ls8 ws0">
              Any breach of these Intell
              <span className="_ _0" />
              ectual Property Rights will <span className="_ _0" />
              constitute a material breac
              <span className="_ _0" />
              h of our Legal Terms and your
              <span className="_ _0" /> right to use o
              <span className="ls9">ur Services will </span>
            </div>
            <div className="t m0 x1 h5 y4f ff4 fs1 fc1 sc0 ls6 ws0">
              terminate immediately.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h6 y50 ff5 fs3 fc0 sc0 ls10 ws0">
              Your submission
              <span className="_ _1" />
              s and contr
              <span className="_ _1" />
              ibutions
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y51 ff4 fs1 fc1 sc0 ls8 ws0">
              Please review this section a
              <span className="_ _0" />
              nd the "<span className="fc2">PROHIBITED ACTIVITIES</span>
              <span className="ls7">
                " section carefully prior to using our Services to understand
                the (a) rights you give{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y52 ff4 fs1 fc1 sc0 ls9 ws0">
              us and (b) obligations you have when you post or upload{" "}
              <span className="_ _0" />
              any content through the Services.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y53 ff2 fs1 fc1 sc0 ls8 ws0">
              Submissions:{" "}
              <span className="ff4">
                By directly sending <span className="_ _0" />
                us any question, comment, sugge
                <span className="_ _0" />
                stion, idea, feedback, or <span className="_ _0" />
                other information about th
                <span className="_ _0" />e Services{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y54 ff4 fs1 fc1 sc0 lsa ws0">
              ("Submissions"), you agree to assign to us all intellectual
              property rights in such Submission. You agree that we shall own t
              <span className="_ _3" />
              <span className="ls9">
                his Submission an
                <span className="ls2">d be </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y55 ff4 fs1 fc1 sc0 ls9 ws0">
              entitled to its unrestricted use and di
              <span className="_ _0" />
              ssemination for any lawful purpose, commercial or otherwise,
              <span className="_ _0" /> without acknowledgment or
              <span className="ls1">
                {" "}
                <span className="lsb">compensation to </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y56 ff4 fs1 fc1 sc0 lsb ws0">
              you.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y57 ff2 fs1 fc1 sc0 ls5 ws0">
              Contributions:{" "}
              <span className="ff4 ls2">
                The Services may <span className="_ _0" />
                invite you to chat, contribute t
                <span className="_ _0" />
                o, or participate in blogs, message boar
                <span className="_ _0" />
                ds, online forums, and other functionality{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y58 ff4 fs1 fc1 sc0 ls9 ws0">
              during which you may create, submit, post, display,{" "}
              <span className="_ _0" />
              transmit, publish, distribute, or broadcast
              <span className="_ _0" /> content and materials to us or through
              the <span className="_ _0" />
              Services,{" "}
            </div>
            <div className="t m0 x1 h5 y59 ff4 fs1 fc1 sc0 ls6 ws0">
              including but not limited to text, w
              <span className="_ _1" />
              ritings, video, audio, photogra
              <span className="_ _1" />
              phs, music, graphics, comm
              <span className="_ _1" />
              ents, reviews, rating sugge
              <span className="_ _1" />
              stion
              <span className="lsb">s, personal </span>
            </div>
            <div className="t m0 x1 h5 y5a ff4 fs1 fc1 sc0 ls6 ws0">
              information, or other material ("
              <span className="_ _1" />
              Contributions"). Any Submis
              <span className="_ _1" />
              sion that is publicly posted sh
              <span className="_ _1" />
              all
              <span className="ls1">
                {" "}
                <span className="ls9">
                  also be treated as a Contribution.
                </span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y5b ff4 fs1 fc1 sc0 ls8 ws0">
              You understand that Contribut
              <span className="_ _0" />
              ions may be viewable by other <span className="_ _0" />
              users of the Services and p
              <span className="_ _0" />
              ossibly through third
              <span className="ls1">
                -<span className="ls9">party websites.</span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y5c ff2 fs1 fc1 sc0 ls4 ws0">
              When you post Co
              <span className="_ _0" />
              ntributions, <span className="_ _0" />
              you grant us a <span className="_ _0" />
              license (incl
              <span className="_ _0" />
              uding use of <span className="_ _0" />
              your name, trad
              <span className="_ _0" />
              emarks, and log
              <span className="_ _0" />
              os):{" "}
              <span className="ff4 ls8">
                By posting any Contri
                <span className="_ _0" />
                butions,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y5d ff4 fs1 fc1 sc0 lsb ws0">
              you grant us an unrestricted, unlimited, ir
              <span className="_ _0" />
              revocable, perpetual, non
              <span className="ls1">
                -<span className="ls9">exclusive, transferable, royalty</span>-{" "}
                <span className="ls6">free, fully</span>-
                <span className="ls9">
                  paid, worldwide right, and license to:{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y5e ff4 fs1 fc1 sc0 ls9 ws0">
              use, copy, reproduce, distribute, sell, r
              <span className="_ _0" />
              esell, publish, broadcast, reti
              <span className="ls6">
                tle, store, publicly perform, publicly display, reformat, tran
                <span className="_ _1" />
                slate, excerpt (in{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y5f ff4 fs1 fc1 sc0 ls5 ws0">
              whole or in part), <span className="_ _0" />
              and exploit your Contr
              <span className="_ _0" />
              ibutions (including,
              <span className="_ _0" /> without limitati
              <span className="_ _0" />
              on, your image, name, an
              <span className="_ _0" />
              d voice) for any purpo
              <span className="_ _0" />s
              <span className="ls9">e, commercial, </span>
            </div>
            <div className="t m0 x1 h5 y60 ff4 fs1 fc1 sc0 ls9 ws0">
              advertising, or otherwise, to prepa
              <span className="lsa">
                re derivative works of, or incorporate into other works, your
                Contributions, and to sublicense the licenses{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y61 ff4 fs1 fc1 sc0 ls9 ws0">
              granted in this section. Our use and distri
              <span className="_ _0" />
              bution may occur in any media formats and through any media
              channels.
              <span className="_ _0" />
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y62 ff4 fs1 fc1 sc0 ls2 ws0">
              This license includes our use of your <span className="_ _0" />
              name, company name, and franchise name, as applicable,
              <span className="_ _0" /> and any of the trademarks, servi
              <span className="lsb">ce marks, trade </span>
            </div>
            <div className="t m0 x1 h5 y63 ff4 fs1 fc1 sc0 ls9 ws0">
              names, logos, and personal and commercial images you provide.
              <span className="ls1"> </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="3" id="pf3">
          <div className="pc pc3 w0 h0">
            <div className="t m0 x1 h5 y35 ff2 fs1 fc1 sc0 ls8 ws0">
              You are responsible for what you post or
              <span className="_ _0" /> upload:{" "}
              <span className="ff4">
                By sending us Submi
                <span className="_ _0" />
                ssions and/or posting Contri
                <span className="_ _0" />
                butions through any part of
                <span className="_ _0" /> the Services or{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y36 ff4 fs1 fc1 sc0 lse ws0">
              making Contributi
              <span className="_ _0" />
              ons accessible t
              <span className="_ _0" />
              hrough the Servi
              <span className="_ _0" />
              ces by linking <span className="_ _0" />
              your account thr
              <span className="_ _0" />
              ough the Service
              <span className="_ _0" />
              s to any of you
              <span className="_ _0" />r social netwo
              <span className="lsa">rking accounts, </span>
            </div>
            <div className="t m0 x1 h5 y64 ff4 fs1 fc1 sc0 lsb ws0">
              you:
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y38 ff4 fs1 fc1 sc0 lsb ws0">
              confirm that you have read and agree with our "
              <span className="fc2 ls8">PROHIBITED ACTIVITIES</span>
              <span className="ls7">
                " and will not post, send, publish, upload, or transmit through
                the{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y39 ff4 fs1 fc1 sc0 ls8 ws0">
              Services any Submission nor pos
              <span className="_ _0" />
              t any Contribution that is
              <span className="_ _0" /> illegal, harassing, hate
              <span className="_ _0" />
              ful, harmful, defamatory, o
              <span className="_ _0" />
              bscene, bullying
              <span className="ls6">, abusive, </span>
            </div>
            <div className="t m0 x1 h5 y65 ff4 fs1 fc1 sc0 ls9 ws0">
              discriminatory, threatening to any person or group,
              <span className="_ _0" />{" "}
              <span className="lsb">
                sexually explicit, false, inaccurate, <span className="_ _0" />
                deceitful, or misleading;
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y66 ff4 fs1 fc1 sc0 ls6 ws0">
              to the extent permissible by a
              <span className="_ _1" />
              pplicable law, waive any an
              <span className="_ _1" />
              d all moral rights to any such S
              <span className="_ _1" />
              ubmission and/or Contribu
              <span className="_ _1" />
              tion;
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y67 ff4 fs1 fc1 sc0 ls5 ws0">
              warrant that any such <span className="_ _0" />
              Submission and/or Contri
              <span className="_ _0" />
              butions are original
              <span className="_ _0" /> to you or that you{" "}
              <span className="_ _0" />
              have the necessary righ
              <span className="_ _0" />
              ts and licenses{" "}
              <span className="ls6">
                to submit s<span className="ls2">uch </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y68 ff4 fs1 fc1 sc0 ls8 ws0">
              Submissions and/or Contributio
              <span className="_ _0" />
              ns and that you have full <span className="_ _0" />
              authority to grant us the
              <span className="_ _0" /> above
              <span className="ls1">
                -{" "}
                <span className="lse">
                  mentioned rights in <span className="_ _0" />
                  relation to yo
                  <span className="_ _0" />
                  ur Submissions and/
                  <span className="_ _0" />
                  or{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y69 ff4 fs1 fc1 sc0 ls5 ws0">
              Contributions; and war
              <span className="_ _0" />
              rant and represent tha
              <span className="_ _0" />
              t your Submissions and/o
              <span className="_ _0" />
              r Contributions do not
              <span className="_ _0" /> constitute confi
              <span className="ls9">
                dent
                <span className="_ _0" />
                ial information.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y3d ff4 fs1 fc1 sc0 ls8 ws0">
              You are solely responsible <span className="_ _0" />
              for your Submissions and/or Con
              <span className="_ _0" />
              tributions and you expressl
              <span className="_ _0" />
              y agree to reimburse us for
              <span className="_ _0" /> any and all{" "}
              <span className="ls6">losses that we may </span>
            </div>
            <div className="t m0 x1 h5 y6a ff4 fs1 fc1 sc0 lsb ws0">
              suffer because of your breach of (a) this secti
              <span className="_ _0" />
              on, (b) any third party’s intellectual
              <span className="_ _0" /> property rights, or (c) ap
              <span className="ls9">
                plicable law.
                <span className="ff6 fs4 fc0 ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y3f ff2 fs1 fc1 sc0 ls4 ws0">
              We may remove or <span className="_ _0" />
              edit your Cont
              <span className="_ _0" />
              ent:{" "}
              <span className="ff4 ls8">
                Although we have no <span className="_ _0" />
                obligation to monitor any Con
                <span className="_ _0" />
                tributions, we shall have t
                <span className="_ _0" />
                he right to remove or edit{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y6b ff4 fs1 fc1 sc0 ls9 ws0">
              any Contributions at any time without notice i
              <span className="_ _0" />
              f in our reasonable opinion we consider such Contribut
              <span className="_ _0" />
              ions harmful or in breach
              <span className="ls1"> </span>
              of these Legal{" "}
            </div>
            <div className="t m0 x1 h5 y6c ff4 fs1 fc1 sc0 ls2 ws0">
              Terms. If we
              <span className="ls1">
                {" "}
                <span className="lsa">
                  remove or edit any such Contributions, we may also suspend or
                  disable your account and report you to the authorities.
                  <span className="_ _3" />
                </span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h4 y6d ff3 fs2 fc0 sc0 lsd ws0">
              3. USER REPRESENTATIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y6e ff4 fs1 fc1 sc0 ls8 ws0">
              By using the Services, you r
              <span className="_ _0" />
              epresent and warrant that: <span className="_ _0" />
              (1) all registration i
              <span className="_ _0" />
              nformation you submit will be <span className="_ _0" />
              true, accurate, cu
              <span className="lsa">rrent, and complete; (2) </span>
            </div>
            <div className="t m0 x1 h5 y6f ff4 fs1 fc1 sc0 lsb ws0">
              you will maintain the accuracy of such information{" "}
              <span className="_ _0" />
              and promptly update such registration information as{" "}
              <span className="_ _0" />
              ne
              <span className="_ _1" />
              cessary; (3) you have the legal{" "}
            </div>
            <div className="t m0 x1 h5 y70 ff4 fs1 fc1 sc0 lsb ws0">
              capacity and you agree to comply with these Legal Terms; (4)
              <span className="_ _0" /> you are not a minor in the jurisdiction{" "}
              <span className="_ _0" />
              in which you reside; (5)
              <span className="ls1"> </span>
              you will not access{" "}
            </div>
            <div className="t m0 x1 h5 y71 ff4 fs1 fc1 sc0 ls6 ws0">
              the Services through autom
              <span className="_ _1" />
              ated or non
              <span className="ls1">
                -
                <span className="ls9">
                  human means, whether through a bot, script or otherwise; (6)
                  you will not
                  <span className="_ _0" /> use the Services for any illegal{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y72 ff4 fs1 fc1 sc0 ls9 ws0">
              or unauthorized purpose; and (7) your use of the Servi
              <span className="_ _0" />
              ces will not violate any applicable law or{" "}
              <span className="_ _0" />
              regulation.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y73 ff4 fs1 fc1 sc0 ls6 ws0">
              If you provide any information
              <span className="_ _1" /> that is untrue, inaccurate, not c
              <span className="_ _1" />
              urrent, or incomplete, we ha
              <span className="_ _1" />
              ve the right to suspend or term
              <span className="_ _1" />
              in
              <span className="ls9">ate your account and </span>
            </div>
            <div className="t m0 x1 h5 y74 ff4 fs1 fc1 sc0 lsa ws0">
              refuse any and all current or future use of the Services (or any
              portion thereof).
              <span className="_ _1" />
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y75 ff3 fs2 fc0 sc0 lsd ws0">
              4. USER REGISTRATION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y76 ff4 fs1 fc1 sc0 ls8 ws0">
              You may be required to regis
              <span className="_ _0" />
              ter to use the Services. Yo
              <span className="_ _0" />u agree to keep your password{" "}
              <span className="_ _0" />
              confidential and will be r
              <span className="_ _0" />
              esponsible for
              <span className="ls1">
                {" "}
                <span className="ls9">all use of your </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y77 ff4 fs1 fc1 sc0 ls9 ws0">
              account and password. We reserve the right to remove, reclai
              <span className="_ _0" />
              m, or change a username you select if we determine, in
              <span className="_ _0" />
              <span className="ls1">
                {" "}
                <span className="ls9">our sole discretion, that </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y78 ff4 fs1 fc1 sc0 lsb ws0">
              such username is inappropriate, obscene, or otherwise object
              <span className="_ _0" />
              ionable.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y79 ff3 fs2 fc0 sc0 lsd ws0">
              5. PURCHASES AND PAYMENT
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y1a ff4 fs1 fc1 sc0 ls4 ws0">
              We accept the <span className="_ _0" />
              following for
              <span className="_ _0" />
              ms of payment:
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y7a ff4 fs1 fc1 sc0 ls8 ws0">
              You agree to provide curren
              <span className="_ _0" />
              t, complete, and accurate pu
              <span className="_ _0" />
              rchase and account informatio
              <span className="_ _0" />
              n for all purchases made via
              <span className="_ _0" /> the Services
              <span className="ls6">. You further </span>
            </div>
            <div className="t m0 x1 h5 y7b ff4 fs1 fc1 sc0 ls9 ws0">
              agree to promptly update account and payment information, i
              <span className="_ _0" />
              ncluding email address, payment method, and payment card e
              <span className="lsb">
                xpirati
                <span className="_ _0" />
                on date, so that{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y7c ff4 fs1 fc1 sc0 ls5 ws0">
              we can complete your tr
              <span className="_ _0" />
              ansactions and contact <span className="_ _0" />
              you as needed. Sales ta
              <span className="_ _0" />
              x will be added to t
              <span className="_ _0" />
              he price of purchases <span className="_ _0" />
              as deemed requ
              <span className="ls6">ired by us. We </span>
            </div>
            <div className="t m0 x1 h5 y7d ff4 fs1 fc1 sc0 lse ws0">
              may change prices
              <span className="_ _0" /> at any time. Al
              <span className="_ _0" />
              l payments shall
              <span className="_ _0" /> be in __________
              <span className="_ _0" />.<span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y7e ff4 fs1 fc1 sc0 ls8 ws0">
              You agree to pay all charge
              <span className="_ _0" />
              s at the prices then in e
              <span className="_ _0" />
              ffect for your purchases a
              <span className="_ _0" />
              nd any applicable shipping f
              <span className="_ _0" />
              ees, and you author
              <span className="ls6">ize us to charge your </span>
            </div>
            <div className="t m0 x1 h5 y7f ff4 fs1 fc1 sc0 lsb ws0">
              chosen payment provider for any such amounts upon placing your
              order.
              <span className="_ _0" /> We reserve the right to correct any err
              <span className="ls9">ors or mistakes in pricing, even if </span>
            </div>
            <div className="t m0 x1 h5 y80 ff4 fs1 fc1 sc0 ls5 ws0">
              we have already requested
              <span className="_ _0" /> or received payment.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y81 ff4 fs1 fc1 sc0 ls4 ws0">
              We reserve the <span className="_ _0" />
              right to ref
              <span className="_ _0" />
              use any order <span className="_ _0" />
              placed through
              <span className="_ _0" /> the Services.
              <span className="_ _0" /> We may, in o
              <span className="_ _0" />
              ur sole discre
              <span className="_ _0" />
              tion, limit
              <span className="_ _0" /> or cancel qua
              <span className="_ _0" />
              ntit
              <span className="ls6">ies purchased per </span>
            </div>
            <div className="t m0 x1 h5 y82 ff4 fs1 fc1 sc0 ls9 ws0">
              person, per household, or per order. These restrict
              <span className="_ _0" />
              ions may include orders placed by or under the same customer a
              <span className="_ _0" />
              <span className="lsb">ccount, the same payment </span>
            </div>
            <div className="t m0 x1 h5 y83 ff4 fs1 fc1 sc0 lse ws0">
              method, and/or o
              <span className="_ _0" />
              rders that use <span className="_ _0" />
              the same billing
              <span className="_ _0" /> or shipping add
              <span className="_ _0" />
              ress. We reserve <span className="_ _0" />
              the right to l
              <span className="_ _0" />
              imit or prohibi
              <span className="_ _0" />
              t orders that,
              <span className="_ _0" /> i
              <span className="ls9">n our sole judgment, </span>
            </div>
            <div className="t m0 x1 h5 y84 ff4 fs1 fc1 sc0 ls9 ws0">
              appear to be placed by dealers, resellers, or di
              <span className="_ _0" />
              stributors.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y85 ff3 fs2 fc0 sc0 lsd ws0">
              6. POLICY
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y86 ff4 fs1 fc1 sc0 ls8 ws0">
              All sales are final and n
              <span className="_ _0" />o refund will be issued.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y87 ff3 fs2 fc0 sc0 lsd ws0">
              7. PROHIBITED ACTIVITIES
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y88 ff4 fs1 fc1 sc0 ls8 ws0">
              You may not access or use the <span className="_ _0" />
              Services for any purpose ot
              <span className="_ _0" />
              her than that for which we mak
              <span className="_ _0" />
              e the Services available. Th
              <span className="_ _0" />e Services
              <span className="ls1">
                {" "}
                <span className="lse">may not be used </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y89 ff4 fs1 fc1 sc0 ls6 ws0">
              in connection with any com
              <span className="_ _1" />
              mercial endeavors excep
              <span className="_ _1" />
              t those that are specifically en
              <span className="_ _1" />
              dorsed or approved by us
              <span className="_ _1" />.<span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y8a ff4 fs1 fc1 sc0 ls8 ws0">
              As a user of the Services, <span className="_ _0" />
              you agree not to:
              <span className="ls1"> </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="4" id="pf4">
          <div className="pc pc4 w0 h0">
            <div className="t m0 x1 h5 y35 ff4 fs1 fc1 sc0 ls8 ws0">
              Systematically retrieve dat
              <span className="_ _0" />
              a or other content from th
              <span className="_ _0" />
              e Services to create or compi
              <span className="_ _0" />
              le, directly or indirec
              <span className="_ _0" />
              tly, a collection, c
              <span className="ls9">
                ompilation, database,
                <span className="_ _0" /> or{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y36 ff4 fs1 fc1 sc0 ls9 ws0">
              directory without written permission from us.
              <span className="ff6 fs4 fc0 ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y37 ff4 fs1 fc1 sc0 ls2 ws0">
              Trick, defraud, or mislead us and other
              <span className="_ _0" /> users, especially in any attempt t
              <span className="_ _0" />o learn sensitive account information{" "}
              <span className="_ _0" />
              such as user{" "}
              <span className="ls9">
                passwords.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y38 ff4 fs1 fc1 sc0 ls5 ws0">
              Circumvent, disable, o
              <span className="_ _0" />
              r otherwise interfer
              <span className="_ _0" />e with security
              <span className="ls1">
                -
                <span className="lsa">
                  related features of the Services, including features that prev
                  <span className="ls9">ent or restrict the use or </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y39 ff4 fs1 fc1 sc0 lsb ws0">
              copying of any Content or enforce limitations on{" "}
              <span className="_ _0" />
              the use of the Services and/or the Content contai
              <span className="_ _0" />
              ned therein.
              <span className="_ _1" />
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y8b ff4 fs1 fc1 sc0 ls5 ws0">
              Disparage, tarnish, o
              <span className="_ _0" />r otherwise harm, in{" "}
              <span className="_ _0" />
              our opinion, us and/or
              <span className="_ _0" /> the Services. Use any
              <span className="_ _0" /> information obtained{" "}
              <span className="_ _0" />
              from the Services
              <span className="ls1">
                {" "}
                <span className="ls6">in order to harass, </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y66 ff4 fs1 fc1 sc0 ls9 ws0">
              abuse, or harm another person.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y67 ff4 fs1 fc1 sc0 lse ws0">
              Make improper use <span className="_ _0" />
              of our support <span className="_ _0" />
              services or sub
              <span className="_ _0" />
              mit false report
              <span className="_ _0" />s of abuse or m
              <span className="ls6">
                isconduct.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y8c ff4 fs1 fc1 sc0 ls5 ws0">
              Use the Services in a <span className="_ _0" />
              manner inconsistent wit
              <span className="_ _0" />h any applicable laws{" "}
              <span className="_ _0" />
              or regulations.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y8d ff4 fs1 fc1 sc0 ls8 ws0">
              Engage in unauthorized framin
              <span className="_ _0" />
              g of or linking to the Se
              <span className="_ _0" />
              rvices.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y3d ff4 fs1 fc1 sc0 ls5 ws0">
              Upload or transmit (or
              <span className="_ _0" /> attempt to upload o
              <span className="_ _0" />
              r to transmit) viru
              <span className="_ _0" />
              ses, Trojan horses, or
              <span className="_ _0" /> other material, in
              <span className="_ _0" />
              cluding e
              <span className="lsb">
                xcessive use of capital l
                <span className="_ _0" />
                etters and{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y6a ff4 fs1 fc1 sc0 lsb ws0">
              spamming (continuous posting of repetitive text),{" "}
              <span className="_ _0" />
              that interferes with any party’s uninterr
              <span className="_ _0" />
              upted use and enjoyment of the Services or modifies,{" "}
            </div>
            <div className="t m0 x1 h5 y8e ff4 fs1 fc1 sc0 ls6 ws0">
              impairs, disrupts, alters, or inter
              <span className="_ _1" />
              feres with the use, features, fun
              <span className="_ _1" />
              ctions,{" "}
              <span className="ls9">
                operation, or maintenance of the Services.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y6b ff4 fs1 fc1 sc0 ls8 ws0">
              Engage in any automated use of
              <span className="_ _0" /> the system, such as using sc
              <span className="_ _0" />
              ripts to send comments or messag
              <span className="_ _0" />
              es, or using any data mining
              <span className="_ _0" />, robo
              <span className="ls6">ts, or similar </span>
            </div>
            <div className="t m0 x1 h5 y6c ff4 fs1 fc1 sc0 ls9 ws0">
              data gathering and extraction tools.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y8f ff4 fs1 fc1 sc0 ls5 ws0">
              Delete the copyright <span className="_ _0" />
              or other proprietary <span className="_ _0" />
              rights notice from <span className="_ _0" />
              any Content. Attem
              <span className="ls9">
                pt to <span className="_ _0" />
                impersonate another user or person or use the username of{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y90 ff4 fs1 fc1 sc0 ls9 ws0">
              another user.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y91 ff4 fs1 fc1 sc0 ls5 ws0">
              Upload or transmit (or
              <span className="_ _0" /> attempt to upload o
              <span className="_ _0" />
              r to transmit) any mat
              <span className="_ _0" />
              erial that acts as
              <span className="_ _0" /> a passive or active{" "}
              <span className="_ _0" />
              information collecti
              <span className="_ _0" />
              on
              <span className="ls1">
                {" "}
                <span className="ls9">or transmission </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y92 ff4 fs1 fc1 sc0 lse ws0">
              mechanism, includi
              <span className="_ _0" />
              ng without limi
              <span className="_ _0" />
              tation, clear g
              <span className="_ _0" />
              raphics interch
              <span className="_ _0" />
              ange formats ("g
              <span className="_ _0" />
              ifs"), 1×1 pixe
              <span className="_ _0" />
              ls, web bugs, co
              <span className="_ _0" />
              okie
              <span className="lsb">s, or other similar devices </span>
            </div>
            <div className="t m0 x1 h5 y93 ff4 fs1 fc1 sc0 lsa ws0">
              (sometimes referred to as "spyware" or "passive collection
              mechanisms" or "pcm
              <span className="_ _1" />
              s").
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y94 ff4 fs1 fc1 sc0 ls6 ws0">
              Interfere with, disrupt, or create
              <span className="_ _1" /> an undue burden on the Se
              <span className="_ _1" />
              rvices or the networks or s
              <span className="_ _1" />
              ervices connected to the Se
              <span className="_ _1" />
              rvices.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y95 ff4 fs1 fc1 sc0 ls5 ws0">
              Harass, annoy, intimid
              <span className="_ _0" />
              ate, or threaten any <span className="_ _0" />
              of our employees or age
              <span className="_ _0" />
              nts engaged in providi
              <span className="_ _0" />
              ng any portion of th
              <span className="_ _0" />e Services to you
              <span className="ls1">. </span>
            </div>
            <div className="t m0 x1 h5 y96 ff4 fs1 fc1 sc0 ls8 ws0">
              Attempt to bypass any measures <span className="_ _0" />
              of the Services designed to <span className="_ _0" />
              prevent or restrict acces
              <span className="_ _0" />s to the Services, or any{" "}
              <span className="_ _0" />
              portion of the Services.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y97 ff4 fs1 fc1 sc0 ls5 ws0">
              Copy or adapt the Servi
              <span className="_ _0" />
              ces' software, incl
              <span className="_ _0" />
              uding but not limit
              <span className="_ _0" />
              ed to Flash, PHP, HTML,
              <span className="_ _0" /> JavaScript, or other
              <span className="_ _0" /> code.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y98 ff4 fs1 fc1 sc0 ls8 ws0">
              Except as permitted by appli
              <span className="_ _0" />
              cable law, decipher, decompile
              <span className="_ _0" />
              , disassemble, or reverse en
              <span className="_ _0" />
              gineer any of the software <span className="_ _0" />
              comprising o<span className="lsa">r in any way </span>
            </div>
            <div className="t m0 x1 h5 y99 ff4 fs1 fc1 sc0 lse ws0">
              making up a part
              <span className="_ _0" /> of the Services
              <span className="_ _0" />.<span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y9a ff4 fs1 fc1 sc0 ls8 ws0">
              Except as may be the result <span className="_ _0" />
              of standard search engine or
              <span className="_ _0" /> Internet browser usage, us
              <span className="_ _0" />e
              <span className="ls6">
                , launch, develop, o
                <span className="_ _1" />
                r distribute any automated sy
                <span className="_ _1" />
                stem,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y9b ff4 fs1 fc1 sc0 ls6 ws0">
              including without limitation, any
              <span className="_ _1" /> spider, robot, cheat utility, scrap
              <span className="_ _1" />
              er, or offline reader that acces
              <span className="_ _1" />
              ses the Services, or use
              <span className="ls1">
                {" "}
                <span className="ls9">or launch any unauthorized </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y9c ff4 fs1 fc1 sc0 lsb ws0">
              script or other software.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y9d ff4 fs1 fc1 sc0 ls5 ws0">
              Use a buying agent or <span className="_ _0" />
              purchasing agent to make <span className="_ _0" />
              purchases on the Service
              <span className="_ _0" />
              s. Make any unauthorized <span className="_ _0" />
              use of the Services, <span className="_ _0" />
              includin
              <span className="ls9">g collecting </span>
            </div>
            <div className="t m0 x1 h5 y9e ff4 fs1 fc1 sc0 ls9 ws0">
              usernames and/or email addresses of users by electronic or ot
              <span className="_ _0" />
              her means for the purpose of sending unsolicited email,
              <span className="_ _0" /> o
              <span className="lsa">r creating user accounts </span>
            </div>
            <div className="t m0 x1 h5 y9f ff4 fs1 fc1 sc0 ls9 ws0">
              by automated means or under false pretenses.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ya0 ff4 fs1 fc1 sc0 ls5 ws0">
              Use the Services as par
              <span className="_ _0" />
              t of any effort t
              <span className="_ _0" />
              o compete with us or o
              <span className="_ _0" />
              therwise use the Servi
              <span className="_ _0" />
              ces and/or the Content <span className="_ _0" />
              for any revenue
              <span className="ls1">
                -<span className="ls9">generating endeavor </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ya1 ff4 fs1 fc1 sc0 ls9 ws0">
              or commercial enterprise.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 ya2 ff3 fs2 fc0 sc0 lsd ws0">
              8. USER GENERATED CONTRIBUTIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ya3 ff4 fs1 fc1 sc0 ls2 ws0">
              The Services may invite you to chat, <span className="_ _0" />
              contribute to, or participate in <span className="_ _0" />
              blogs, message boards, online forums, and other
              <span className="_ _0" /> functio
              <span className="ls9">nality, and may </span>
            </div>
            <div className="t m0 x1 h5 ya4 ff4 fs1 fc1 sc0 ls9 ws0">
              provide you with the opportunity to create,{" "}
              <span className="_ _0" />
              submit, post, display, transmit, perform, publ
              <span className="_ _0" />
              ish, distribute, or broad
              <span className="lsb">
                cast content and mater
                <span className="_ _0" />
                ials to us or{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 ya5 ff4 fs1 fc1 sc0 ls9 ws0">
              on the Services, including but not limited{" "}
              <span className="_ _0" />
              to text, writings, video, audio, photogr
              <span className="_ _0" />
              aphs, graphics, comments, suggestions, or personal informati
              <span className="_ _0" />
              on or{" "}
            </div>
            <div className="t m0 x1 h5 ya6 ff4 fs1 fc1 sc0 ls9 ws0">
              other material (collectively, "Contributi
              <span className="_ _0" />
              ons"). Contributions may be viewable by other users of
              <span className="_ _0" /> the Services and through third
              <span className="ls1">-</span>
              party websites. As{" "}
            </div>
            <div className="t m0 x1 h5 ya7 ff4 fs1 fc1 sc0 lsb ws0">
              such, any Contributions you transmit may be treated as{" "}
              <span className="_ _0" />
              non
              <span className="ls1">-</span>
              confidential and non
              <span className="ls1">
                -{" "}
                <span className="ls9">
                  proprietary. When you create or make available any{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ya8 ff4 fs1 fc1 sc0 ls5 ws0">
              Contributions, you t
              <span className="_ _0" />
              hereby represent and warr
              <span className="_ _0" />
              ant{" "}
              <span className="ls6">
                that:
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ya9 ff4 fs1 fc1 sc0 ls2 ws0">
              The creation, distribution, transmi
              <span className="_ _0" />
              ssion, public display, or performance,
              <span className="_ _0" /> and the accessing, downloading, or
              copying of
              <span className="_ _0" /> your{" "}
              <span className="ls5">Contributions do not </span>
            </div>
            <div className="t m0 x1 h5 yaa ff4 fs1 fc1 sc0 ls9 ws0">
              and will not infringe the proprietary r
              <span className="_ _0" />
              ights, including but not limited to t
              <span className="_ _0" />
              he copyright, patent, trademark, t
              <span className="lsa">
                rade secret, or moral rights of any third{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yab ff4 fs1 fc1 sc0 ls9 ws0">
              party.
              <span className="ff6 fs4 fc0 ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yac ff4 fs1 fc1 sc0 ls8 ws0">
              You are the creator and owner
              <span className="_ _0" /> of or have the necessary{" "}
              <span className="_ _0" />
              licenses, rights, consents,
              <span className="_ _0" /> releases, and permissions t
              <span className="_ _0" />o use and to au
              <span className="ls6">thorize us, the </span>
            </div>
            <div className="t m0 x1 h5 yad ff4 fs1 fc1 sc0 ls8 ws0">
              Services, and other users o
              <span className="_ _0" />f the Services to use your{" "}
              <span className="_ _0" />
              Contributions in any manner c
              <span className="_ _0" />
              ontemplated by the Services an
              <span className="_ _0" />d{" "}
              <span className="ls6">
                these Legal Term
                <span className="_ _1" />
                s.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yae ff4 fs1 fc1 sc0 ls8 ws0">
              You have the written consent,
              <span className="_ _0" /> release, and/or permission
              <span className="_ _0" /> of each and every identif
              <span className="_ _0" />
              iable individual person in
              <span className="_ _0" /> your Contributio
              <span className="ls9">ns to use the name or </span>
            </div>
            <div className="t m0 x1 h5 yaf ff4 fs1 fc1 sc0 ls6 ws0">
              likeness of each and every
              <span className="_ _1" /> such identifiable individual pe
              <span className="_ _1" />
              rson to enable inclusion an
              <span className="_ _1" />d use
              <span className="ls1">
                {" "}
                <span className="ls9">
                  of your Contributions in any manner contemplated by{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yb0 ff4 fs1 fc1 sc0 ls6 ws0">
              the Services and these Leg
              <span className="_ _1" />
              al Terms.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yb1 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions are not <span className="_ _0" />
              false, inaccurate, or misl
              <span className="_ _0" />
              eading.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yb2 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions are not <span className="_ _0" />
              unsolicited or unauthorized
              <span className="_ _0" /> advertising, promotional mat
              <span className="_ _0" />
              erials, pyramid schemes
              <span className="ls6">
                , chain letters, spam, mass mailings,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yb3 ff4 fs1 fc1 sc0 ls9 ws0">
              or other forms of solicitation.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yb4 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions are not <span className="_ _0" />
              obscene, lewd, lascivious, f
              <span className="_ _0" />
              ilthy, violent, harassing
              <span className="_ _0" />
              , libelous, slanderous, or
              <span className="_ _0" /> otherwise objectio
              <span className="ls9">nable (as determined by </span>
            </div>
            <div className="t m0 x1 h5 yb5 ff4 fs1 fc1 sc0 ls9 ws0">
              us).
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yb6 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions do not r
              <span className="_ _0" />
              idicu
              <span className="ls6">
                le, mock, disparage, intimidate, o
                <span className="_ _1" />r abuse anyone.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yb7 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions are not <span className="_ _0" />
              used to harass or threaten <span className="_ _0" />
              (in the legal sense of t
              <span className="_ _0" />
              hose terms) any other person <span className="_ _0" />
              and to promote vio
              <span className="ls6">lence against a </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="5" id="pf5">
          <div className="pc pc5 w0 h0">
            <div className="t m0 x1 h5 y35 ff4 fs1 fc1 sc0 lsb ws0">
              specific person or class of people.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y36 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions do not vi
              <span className="_ _0" />
              olate any applicable law, r
              <span className="_ _0" />
              egulation, or rule.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y64 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions do not vi
              <span className="_ _0" />
              olate the privacy or publ
              <span className="_ _0" />
              icity rights of any thir
              <span className="_ _0" />d party.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yb8 ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions do not vi
              <span className="_ _0" />
              olate any applicable law con
              <span className="_ _0" />
              cer
              <span className="ls9">
                ning child pornography, or otherwise intended to protect{" "}
                <span className="_ _0" />
                the health or well
                <span className="ls1">-</span>
                being of{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yb9 ff4 fs1 fc1 sc0 lse ws0">
              minors.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yba ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contributions do not i
              <span className="_ _0" />
              nclude any offensive comments t
              <span className="_ _0" />
              hat are connected to race, <span className="_ _0" />
              national origin, gender, <span className="_ _0" />
              sexual preferen
              <span className="lsb">ce, or physical </span>
            </div>
            <div className="t m0 x1 h5 ybb ff4 fs1 fc1 sc0 ls9 ws0">
              handicap.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ybc ff4 fs1 fc1 sc0 ls8 ws0">
              Your Contrib
              <span className="ls9">
                utions do not otherwise violat
                <span className="_ _0" />
                e, or link to material that violates,
                <span className="_ _0" /> any provision of these Legal Terms, or
                any applicabl
                <span className="_ _0" />e law
                <span className="ls1"> </span>
                or{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 ybd ff4 fs1 fc1 sc0 lsa ws0">
              regulation.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ybe ff4 fs1 fc1 sc0 ls8 ws0">
              Any use of the Services in <span className="_ _0" />
              violation of the foregoin
              <span className="_ _0" />g violates these Legal Terms{" "}
              <span className="_ _0" />
              and may result in, among other
              <span className="_ _0" /> things, termi
              <span className="ls9">nation or suspension </span>
            </div>
            <div className="t m0 x1 h5 ybf ff4 fs1 fc1 sc0 ls9 ws0">
              of your rights to use the Services.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 yc0 ff3 fs2 fc0 sc0 lsd ws0">
              9. CONTRIBUTION LICENSE
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yc1 ff4 fs1 fc1 sc0 ls8 ws0">
              By posting your Contribution
              <span className="_ _0" />
              s to any part of the Servi
              <span className="_ _0" />
              ces or making Contributions a
              <span className="_ _0" />
              ccessible to the Services by
              <span className="_ _0" /> linking your{" "}
              <span className="ls9">account from the </span>
            </div>
            <div className="t m0 x1 h5 yc2 ff4 fs1 fc1 sc0 ls8 ws0">
              Services to any of your soc
              <span className="_ _0" />
              ial networking accounts, you <span className="_ _0" />
              automatically grant, and yo
              <span className="_ _0" />
              u represent and warrant that
              <span className="_ _0" /> yo
              <span className="ls9">u have the right to grant, to us </span>
            </div>
            <div className="t m0 x1 h5 yc3 ff4 fs1 fc1 sc0 ls9 ws0">
              an unrestricted, unlimited, irrevocable, perpet
              <span className="_ _0" />
              ual, non
              <span className="ls1">-</span>
              exclusive, transferable, royalty
              <span className="ls1">
                -<span className="ls6">free, fully</span>-
              </span>
              paid, worldwide right,
              <span className="_ _0" /> and license to host, use,{" "}
            </div>
            <div className="t m0 x1 h5 yc4 ff4 fs1 fc1 sc0 lsb ws0">
              copy, reproduce, disclose, sell, resell, publ
              <span className="_ _0" />
              ish, broadcast, retitle, arc
              <span className="ls9">
                hive, store, cache, publicly perform, publicl
                <span className="_ _0" />y display, reformat, translate,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yc5 ff4 fs1 fc1 sc0 ls6 ws0">
              transmit, excerpt (in whole or
              <span className="_ _1" /> in part), and distribute such C
              <span className="_ _1" />
              ontributions (including, withou
              <span className="_ _1" />
              t limitation, your image and vo
              <span className="_ _1" />i
              <span className="lsb">ce) for any purpose, </span>
            </div>
            <div className="t m0 x1 h5 yc6 ff4 fs1 fc1 sc0 lsb ws0">
              commercial, advertising, or other
              <span className="ls5">
                wise, and to p
                <span className="_ _0" />
                repare derivative works <span className="_ _0" />
                of, or incorporate <span className="_ _0" />
                into other works, suc
                <span className="_ _0" />h Contributions, and{" "}
                <span className="_ _0" />
                grant and{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yc7 ff4 fs1 fc1 sc0 ls9 ws0">
              authorize sublicenses of the foregoing. The use and{" "}
              <span className="_ _0" />
              distribution may occur in any media formats and through any{" "}
              <span className="_ _0" />
              media channel
              <span className="lsb">
                s.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yc8 ff4 fs1 fc1 sc0 ls2 ws0">
              This license will apply to any for
              <span className="_ _0" />
              m, media, or technology now known or hereafter{" "}
              <span className="_ _0" />
              developed, and includes our use of your name
              <span className="ls6">, company name, </span>
            </div>
            <div className="t m0 x1 h5 yc9 ff4 fs1 fc1 sc0 ls9 ws0">
              and franchise name, as applicable, and any of the t
              <span className="_ _0" />
              rademarks, service marks, trade names, logos, and personal and c
              <span className="_ _0" />
              ommercial images you{" "}
            </div>
            <div className="t m0 x1 h5 yca ff4 fs1 fc1 sc0 ls9 ws0">
              provide. You waive all moral rights in your Contr
              <span className="_ _0" />
              ibutions, and you warrant that moral rights have{" "}
              <span className="_ _0" />
              not otherwise been asserted
              <span className="ls1">
                {" "}
                <span className="ls6">in your </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ycb ff4 fs1 fc1 sc0 ls5 ws0">
              Contributions.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ycc ff4 fs1 fc1 sc0 ls4 ws0">
              We do not asser
              <span className="_ _0" />
              t any ownershi
              <span className="_ _0" />
              p over your Con
              <span className="_ _0" />
              tributions. <span className="_ _0" />
              You retain fu
              <span className="_ _0" />
              ll ownership o
              <span className="_ _0" />
              f all of yo
              <span className="_ _0" />
              ur Contributi
              <span className="_ _0" />
              ons and any int
              <span className="_ _0" />
              ell
              <span className="ls9">ectual property rights </span>
            </div>
            <div className="t m0 x1 h5 ycd ff4 fs1 fc1 sc0 ls9 ws0">
              or other proprietary rights associated with your
              <span className="_ _0" /> Contributions. We are not liable for any
              st
              <span className="_ _0" />
              atements or representations in your Contributions{" "}
            </div>
            <div className="t m0 x1 h5 yce ff4 fs1 fc1 sc0 ls9 ws0">
              provided by you in any area on the Services. You are sol
              <span className="_ _0" />
              ely responsible for your Contributions to the{" "}
              <span className="_ _0" />
              Services and you expres
              <span className="lsb">sly agree to </span>
            </div>
            <div className="t m0 x1 h5 ycf ff4 fs1 fc1 sc0 ls9 ws0">
              exonerate us from any and all responsibility and t
              <span className="_ _0" />
              o refrain from any legal action against us r
              <span className="_ _0" />
              egarding your Contributions.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yd0 ff4 fs1 fc1 sc0 ls4 ws0">
              We have the ri
              <span className="_ _0" />
              ght, in our <span className="_ _0" />
              sole and absol
              <span className="_ _0" />
              ute discretio
              <span className="_ _0" />
              n, (1) to edi
              <span className="_ _0" />
              t, redact, <span className="_ _0" />
              or otherwise c
              <span className="_ _0" />
              hange any Contr
              <span className="_ _0" />
              ibutions; (2)
              <span className="_ _0" /> to re
              <span className="ls1">
                -<span className="lsb">categorize any </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yd1 ff4 fs1 fc1 sc0 ls5 ws0">
              Contributions to pla
              <span className="_ _0" />
              ce them in more appropri
              <span className="_ _0" />
              ate locations on the <span className="_ _0" />
              Services; and (3) to
              <span className="_ _0" /> pre
              <span className="ls1">
                -
                <span className="lsb">
                  screen or delete any Cont
                  <span className="lsa">ributions at any time and for </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yd2 ff4 fs1 fc1 sc0 ls9 ws0">
              any reason, without notice. We have no obligation{" "}
              <span className="_ _0" />
              to monitor your Contributions.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 yd3 ff3 fs2 fc0 sc0 lsd ws0">
              10. GUIDELINES FOR REVIEWS
              <span className="ff6 fs4 ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yd4 ff4 fs1 fc1 sc0 ls4 ws0">
              We may provide <span className="_ _0" />
              you areas on t
              <span className="_ _0" />
              he Services to
              <span className="_ _0" /> leave reviews <span className="_ _0" />
              or ratings. <span className="_ _0" />
              When posting a <span className="_ _0" />
              review, you mu
              <span className="_ _0" />
              st comply wit
              <span className="_ _0" />
              h the followi
              <span className="_ _0" />
              <span className="ls9">ng criteria: (1) </span>
            </div>
            <div className="t m0 x1 h5 yd5 ff4 fs1 fc1 sc0 lsb ws0">
              you should have firsthand experience with the person/ent
              <span className="_ _0" />
              ity being reviewed; (2) your reviews should not cont
              <span className="_ _0" />
              ain of
              <span className="_ _1" />
              <span className="ls6">
                fensive profanity, or abusiv
                <span className="_ _1" />
                e,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yd6 ff4 fs1 fc1 sc0 lsa ws0">
              racist, offensive, or hateful language; (3) your reviews should
              not contain discriminatory references based on religion, race
              <span className="_ _3" />
              <span className="ls6">, gender, national </span>
            </div>
            <div className="t m0 x1 h5 yd7 ff4 fs1 fc1 sc0 ls9 ws0">
              origin, age, marital status, sexual orient
              <span className="_ _0" />
              ation, or disability; (4) your reviews <span className="_ _0" />
              <span className="lsb">
                should not contain references to illegal act
                <span className="_ _0" />
                ivity; (5) you should not be{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yd8 ff4 fs1 fc1 sc0 ls9 ws0">
              affiliated with competitors if posti
              <span className="_ _0" />
              ng negative reviews; (6) you should not make any conclusions as{" "}
              <span className="_ _0" />
              to the legality of conduc
              <span className="ls6">t; (7) you may not post </span>
            </div>
            <div className="t m0 x1 h5 yd9 ff4 fs1 fc1 sc0 ls9 ws0">
              any false or misleading statements
              <span className="ls6">
                ; and (8) you may not organize a campaign e
                <span className="_ _1" />
                ncouraging others to post re
                <span className="_ _1" />
                views, whether positive or n
                <span className="_ _1" />
                egative.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yda ff4 fs1 fc1 sc0 ls4 ws0">
              We may accept, <span className="_ _0" />
              reject, or r
              <span className="_ _0" />
              emove reviews i
              <span className="_ _0" />
              n our sole di
              <span className="_ _0" />
              scretion. We h
              <span className="_ _0" />
              ave absolutely
              <span className="_ _0" /> no obligatio
              <span className="_ _0" />
              n to screen r
              <span className="_ _0" />
              eviews or to d
              <span className="_ _0" />
              el
              <span className="ls9">ete reviews, even if </span>
            </div>
            <div className="t m0 x1 h5 ydb ff4 fs1 fc1 sc0 ls9 ws0">
              anyone considers reviews objectionable or inaccurate. Reviews ar
              <span className="_ _0" />
              e not endorsed by us, and do not necessarily represent
              <span className="_ _0" /> our opinions or the{" "}
            </div>
            <div className="t m0 x1 h5 ydc ff4 fs1 fc1 sc0 lsb ws0">
              views of any of our affiliates or part
              <span className="_ _0" />
              ners. We do not assume liability for any review or
              <span className="_ _0" /> for any claims, liabilities, or l
              <span className="_ _0" />
              oss
              <span className="_ _1" />
              <span className="ls9">es resulting from any review. </span>
            </div>
            <div className="t m0 x1 h5 ydd ff4 fs1 fc1 sc0 ls8 ws0">
              By posting a review, you her
              <span className="_ _0" />
              eby grant to us a perpetual
              <span className="_ _0" />, non
              <span className="ls1">
                - <span className="ls9">exclusive,</span>{" "}
                <span className="ls5">worldwide, royalty</span>-
                <span className="ls6">
                  free, fully paid, assignable, and sublicensable right{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yde ff4 fs1 fc1 sc0 ls9 ws0">
              and license to reproduce, modify, translate, t
              <span className="_ _0" />
              ransmit by any means, display, perform, and/or distri
              <span className="_ _0" />
              bute all content relating{" "}
              <span className="ls6">
                to review.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h4 ydf ff3 fs2 fc0 sc0 lsd ws0">
              11. SOCIAL MEDIA
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ye0 ff4 fs1 fc1 sc0 ls8 ws0">
              As part of the functional
              <span className="_ _0" />
              ity of the Services, you may
              <span className="_ _0" /> link your account with onl
              <span className="_ _0" />
              ine accounts you have with th
              <span className="_ _0" />
              ird
              <span className="ls1">
                -
                <span className="ls9">party service providers (each such </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ye1 ff4 fs1 fc1 sc0 ls9 ws0">
              account, a "Third
              <span className="ls1">
                -{" "}
                <span className="ls8">
                  Party Account") by ei
                  <span className="_ _0" />
                  ther: (1) providing your
                  <span className="_ _0" /> Third
                  <span className="ls1">-</span>
                  Party Account login informati
                  <span className="_ _0" />
                  on throu
                  <span className="ls9">
                    gh the Services; or (2) allowing us t
                    <span className="_ _0" />o{" "}
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ye2 ff4 fs1 fc1 sc0 ls9 ws0">
              access your Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Account, as is <span className="_ _0" />
                  permitted under the applicab
                  <span className="_ _0" />
                  le terms and conditions that
                  <span className="_ _0" /> govern your use of each Thi
                  <span className="_ _0" />
                  rd
                  <span className="ls1">-</span>
                  Party Account.{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ye3 ff4 fs1 fc1 sc0 ls8 ws0">
              You represent and warrant th
              <span className="_ _0" />
              at you are entitled to di
              <span className="_ _0" />
              sclose your Third
              <span className="ls1">-</span>
              Party
              <span className="ls1"> </span>
              Account login in
              <span className="_ _0" />
              formation to us and/or gran
              <span className="_ _0" />t us access to your Third
              <span className="ls1">-</span>
            </div>
            <div className="t m0 x1 h5 ye4 ff4 fs1 fc1 sc0 ls8 ws0">
              Party Account, without breach
              <span className="_ _0" /> by you of any of the ter
              <span className="_ _0" />
              ms and conditions that govern
              <span className="_ _0" /> your use of the applicabl
              <span className="_ _0" />e Third
              <span className="ls1">-</span>
              Party Account, and without{" "}
            </div>
            <div className="t m0 x1 h5 ye5 ff4 fs1 fc1 sc0 ls9 ws0">
              obligating us to pay any fees or making us
              <span className="ls1">
                {" "}
                <span className="lsb">
                  subject t
                  <span className="_ _0" />o any usage limitations imposed by
                  the third
                  <span className="ls1">
                    -
                    <span className="ls9">
                      party service provider of the Third
                      <span className="_ _0" />
                      <span className="ls1">
                        -<span className="ls8">Party Account. </span>
                      </span>
                    </span>
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ye6 ff4 fs1 fc1 sc0 ls8 ws0">
              By granting us access to any
              <span className="_ _0" /> Third
              <span className="ls1">-</span>
              Party Accounts, you understa
              <span className="_ _0" />
              nd that (1) we may access, make <span className="_ _0" />
              available, and store (if
              <span className="_ _0" /> applicable) any content{" "}
            </div>
            <div className="t m0 x1 h5 ye7 ff4 fs1 fc1 sc0 ls6 ws0">
              that y
              <span className="ls9">
                ou have provided to and stored in your Third
                <span className="ls1">
                  -
                  <span className="ls8">
                    Party Accoun
                    <span className="_ _0" />
                    t (the "Social Network Content
                    <span className="_ _0" />
                    ") so that it is availa
                    <span className="_ _0" />
                    ble on and through the Servi
                    <span className="_ _0" />
                    ces{" "}
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ye8 ff4 fs1 fc1 sc0 lsb ws0">
              via your account, including without limitat
              <span className="_ _0" />
              ion any friend lists and (2) we may submit to and{" "}
              <span className="_ _0" />
              receive from your Thi
              <span className="lsa">
                rd
                <span className="ls1">
                  -<span className="ls8">Party Account additional </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ye9 ff4 fs1 fc1 sc0 ls6 ws0">
              information to the extent you a
              <span className="_ _1" />
              re notified when you link you
              <span className="_ _1" />r account with the Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Account. Depending on the Third
                </span>
                -<span className="ls8">Party Accounts you </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yea ff4 fs1 fc1 sc0 lsb ws0">
              choose and subject to the privacy settings that{" "}
              <span className="_ _0" />
              you have set in such Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Accou
                  <span className="ls9">
                    nts, personally identifiable information that
                    <span className="_ _0" /> you post to your{" "}
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yeb ff4 fs1 fc1 sc0 ls2 ws0">
              Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Accounts may be availabl
                  <span className="_ _0" />
                  e on and through your accoun
                  <span className="_ _0" />
                  t on the Services. Please no
                  <span className="_ _0" />
                  te that if a Third
                  <span className="ls1">-</span>
                  Party Account or as
                  <span className="_ _0" />
                  sociated{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yec ff4 fs1 fc1 sc0 lsb ws0">
              service becomes unavailable or our access to such Thir
              <span className="ls1">
                d-
                <span className="ls8">
                  Party Account is termin
                  <span className="_ _0" />
                  ated by the third
                  <span className="ls1">
                    -
                    <span className="ls9">
                      party service provider, <span className="_ _0" />
                      then Social Network{" "}
                    </span>
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yed ff4 fs1 fc1 sc0 ls5 ws0">
              Content may no longer be
              <span className="_ _0" /> available on and thr
              <span className="_ _0" />
              ough the Services. You <span className="_ _0" />
              will have the abil
              <span className="_ _0" />
              ity to disable the <span className="_ _0" />
              connection between y
              <span className="ls9">
                our account on t
                <span className="_ _0" />
                he{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yee ff4 fs1 fc1 sc0 ls8 ws0">
              Services and your Third
              <span className="ls1">
                -P
                <span className="ls9">
                  arty Accounts at any ti
                  <span className="_ _0" />
                  me. PLEASE NOTE THAT YOUR RELATIONSHIP WITH THE THIRD
                  <span className="ls1">
                    - <span className="ls8">PARTY SERVICE </span>
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yef ff4 fs1 fc1 sc0 ls8 ws0">
              PROVIDERS ASSOCIATED WITH YOUR THIRD
              <span className="ls1">-</span>
              PARTY ACCOUNTS IS GOVERNED SOLELY BY YOUR AGREEMENT(S) WITH SUCH{" "}
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="6" id="pf6">
          <div className="pc pc6 w0 h0">
            <div className="t m0 x1 h5 y35 ff4 fs1 fc1 sc0 ls2 ws0">
              THIRD
              <span className="ls1">
                -
                <span className="ls8">
                  PARTY SERVICE PROVIDERS. We make no effort to review{" "}
                  <span className="_ _0" />
                  any Social Network Content for <span className="_ _0" />
                  any purpose, including but <span className="_ _0" />
                  not limited to
                  <span className="ls6">, for </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y36 ff4 fs1 fc1 sc0 ls9 ws0">
              accuracy, legality, or non
              <span className="ls1">
                -
                <span className="ls6">
                  infringement, and we are not responsible for
                  <span className="_ _1" /> any Social Network Conte
                  <span className="_ _1" />
                  nt. You acknowledge and
                </span>{" "}
              </span>
              agree that we may{" "}
            </div>
            <div className="t m0 x1 h5 y64 ff4 fs1 fc1 sc0 ls9 ws0">
              access your email address book associated with a Third
              <span className="ls1">
                -
                <span className="ls8">
                  Part
                  <span className="_ _0" />
                  y Account and your contacts l
                  <span className="_ _0" />
                  ist stored on your mobile <span className="_ _0" />
                  device or tablet computer{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yb8 ff4 fs1 fc1 sc0 lsb ws0">
              solely for purposes of identifying and inf
              <span className="_ _0" />
              orming you of those contacts who have also registered t
              <span className="ls9">
                o use the Services. You can deactivate the{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yb9 ff4 fs1 fc1 sc0 lsb ws0">
              connection between the Services and your Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Account by contact
                  <span className="_ _0" />
                  ing us using the contact i
                  <span className="_ _0" />
                  nformation below or through y
                  <span className="_ _0" />
                  our account{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yba ff4 fs1 fc1 sc0 lsb ws0">
              settings (if applicable). We will att
              <span className="_ _0" />
              empt to delete any information stored{" "}
              <span className="ls9">
                on our servers that was obtained through such Third
                <span className="ls1">
                  -
                  <span className="ls8">
                    Party <span className="_ _0" />
                    Account,{" "}
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 ybb ff4 fs1 fc1 sc0 ls9 ws0">
              except the username and profile picture that become associat
              <span className="_ _0" />
              ed with your account.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y66 ff3 fs2 fc0 sc0 lsd ws0">
              12. THIRD
              <span className="ls1">
                -<span className="ls11">PARTY WEBSITES AND CONTENT</span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yf0 ff4 fs1 fc1 sc0 ls2 ws0">
              The Services may contain (or you may be sent
              <span className="_ _0" /> via the Site) links to other{" "}
              <span className="_ _0" />
              websites ("Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Websites") as well as a
                  <span className="_ _0" />
                  rticles, photographs, text,
                  <span className="_ _0" />{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yf1 ff4 fs1 fc1 sc0 ls9 ws0">
              graphics, pictures, designs, music, sound, video,{" "}
              <span className="_ _0" />
              information, applications, software, and other{" "}
              <span className="_ _0" />
              content or{" "}
              <span className="ls6">
                items belonging to or originating from{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yf2 ff4 fs1 fc1 sc0 ls6 ws0">
              third parties ("Third
              <span className="ls1">
                -<span className="ls8">Party Content"). Such Third</span>-
                <span className="ls8">Party Websites and Third</span>-
                <span className="ls8">
                  Part
                  <span className="_ _0" />
                  y Content are not investiga
                  <span className="_ _0" />
                  ted, monitored, or checked f
                  <span className="_ _0" />
                  or accuracy,{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yf3 ff4 fs1 fc1 sc0 ls9 ws0">
              appropriateness, or completeness by us, and we are not responsi
              <span className="_ _0" />
              ble for
              <span className="ls1"> </span>
              any Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Websites accessed <span className="_ _0" />
                  through the Services or any
                  <span className="ff6 fs4 fc0 ls1"> </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yc1 ff4 fs1 fc1 sc0 ls2 ws0">
              Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Content posted on, ava
                  <span className="_ _0" />
                  ilable through, or instal
                  <span className="_ _0" />
                  led from the Services, incl
                  <span className="_ _0" />
                  uding the content, accuracy
                  <span className="_ _0" />, offensiveness, o
                  <span className="ls9">pinions, </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yc2 ff4 fs1 fc1 sc0 lsa ws0">
              reliability, privacy practices, or other policies of or contained
              in the Third
              <span className="_ _1" />
              <span className="ls1">
                -<span className="ls8">Party Websites or the Third</span>-
                <span className="ls8">
                  Party Con
                  <span className="_ _0" />
                  <span className="ls6">
                    tent. Inclusion of, linking to, or{" "}
                  </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yc3 ff4 fs1 fc1 sc0 ls9 ws0">
              permitting the use or installation of <span className="_ _0" />
              any Third
              <span className="ls1">
                -<span className="ls8">Party Websites or any Third</span>-
                <span className="ls8">
                  Party
                  <span className="_ _0" /> Content does not imply approv
                  <span className="_ _0" />
                  al or endorsement thereof by <span className="_ _0" />
                  us. If{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yc4 ff4 fs1 fc1 sc0 lsb ws0">
              you decide to leave the Services and access the Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Websites or t
                  <span className="_ _0" />o
                  <span className="ls1">
                    {" "}
                    <span className="ls9">use or install any Third</span>-
                  </span>
                  Party Content, y
                  <span className="_ _0" />
                  ou do so at your own risk, and{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yc5 ff4 fs1 fc1 sc0 lsb ws0">
              you should be aware these Legal Terms no longer govern. You should
              r
              <span className="_ _0" />
              eview the applicable terms and policies, including{" "}
              <span className="_ _0" />
              privac
              <span className="_ _1" />y and data{" "}
            </div>
            <div className="t m0 x1 h5 yc6 ff4 fs1 fc1 sc0 ls9 ws0">
              gathering practices, of any website to which you{" "}
              <span className="_ _0" />
              navigate from the Services or relating to any appl
              <span className="_ _0" />
              ications you use or install from the Services.{" "}
            </div>
            <div className="t m0 x1 h5 yc7 ff4 fs1 fc1 sc0 ls8 ws0">
              Any purchases you make through <span className="_ _0" />
              Third
              <span className="ls1">-</span>
              Party Websites will be through o
              <span className="_ _0" />
              ther websites and from other <span className="_ _0" />
              companies, and we take no respo
              <span className="_ _0" />
              nsibility{" "}
            </div>
            <div className="t m0 x1 h5 yf4 ff4 fs1 fc1 sc0 ls5 ws0">
              whatsoever in rela
              <span className="ls6">
                tion to such purchases which are exclusively betw
                <span className="_ _1" />
                een you and the applicab
                <span className="_ _1" />
                le third party. You agree and a
                <span className="_ _1" />
                cknowledge that we{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yf5 ff4 fs1 fc1 sc0 ls9 ws0">
              do not endorse the products or services offered on Thi
              <span className="_ _0" />
              rd
              <span className="ls1">
                -
                <span className="ls8">
                  Party Websites and you shall <span className="_ _0" />
                  hold us blameless from any har
                  <span className="_ _0" />m caused by your{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yf6 ff4 fs1 fc1 sc0 ls9 ws0">
              purchase of such products or services. Additionally,{" "}
              <span className="_ _0" />
              you shall hold us blameless from any losses sustained by you{" "}
              <span className="_ _0" />
              or harm cau
              <span className="lsb">sed to you relating </span>
            </div>
            <div className="t m0 x1 h5 yf7 ff4 fs1 fc1 sc0 ls6 ws0">
              to or resulting in any way from
              <span className="_ _1" /> any Third
              <span className="ls1">
                -
                <span className="ls8">
                  Party Content or any contact with Thir
                  <span className="_ _0" />d<span className="ls1">- </span>
                  Party Websites.
                  <span className="ls1"> </span>
                </span>
              </span>
            </div>
            <div className="t m0 x1 h4 yf8 ff3 fs2 fc0 sc0 lsd ws0">
              13. SERVICES MANAGEMENT
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 yf9 ff4 fs1 fc1 sc0 ls4 ws0">
              We reserve the <span className="_ _0" />
              right, but <span className="_ _0" />
              not the oblig
              <span className="_ _0" />
              ation, to: <span className="_ _0" />
              (1) monitor t
              <span className="_ _0" />
              he Services fo
              <span className="_ _0" />r violations <span className="_ _0" />
              of these Lega
              <span className="_ _0" />l Terms; (2) <span className="_ _0" />
              take appr
              <span className="ls9">
                opriate legal act
                <span className="_ _0" />
                ion{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yfa ff4 fs1 fc1 sc0 ls9 ws0">
              against anyone who, in our sole discretion, viol
              <span className="_ _0" />
              ates the law or these Legal Terms, including wit
              <span className="_ _0" />
              hout limitation, reporting such user to law{" "}
            </div>
            <div className="t m0 x1 h5 yfb ff4 fs1 fc1 sc0 ls9 ws0">
              enforcement authorities; (3) in our sole discr
              <span className="_ _0" />
              etion and without limitation, refuse, r
              <span className="_ _0" />
              estrict access to, limit the availabil
              <span className="_ _0" />
              it
              <span className="lsb">y of, or disable (to the extent </span>
            </div>
            <div className="t m0 x1 h5 yfc ff4 fs1 fc1 sc0 ls6 ws0">
              technologically feasible) any
              <span className="_ _1" /> of your Contributions or any
              <span className="_ _1" /> portion th
              <span className="ls9">
                ereof; (4) in our sole discretion and without l
                <span className="_ _0" />
                imitation, notice, or liability,
                <span className="_ _0" /> to{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yfd ff4 fs1 fc1 sc0 lsa ws0">
              remove from the Services or otherwise disable all files and
              content that are excessive in size or are in any way burdens
              <span className="_ _1" />
              ome t
              <span className="_ _1" />
              <span className="ls9">o our systems; and </span>
            </div>
            <div className="t m0 x1 h5 yfe ff4 fs1 fc1 sc0 lsa ws0">
              (5) otherwise manage the Serv
              <span className="ls6">
                ices in a m
                <span className="_ _1" />
                anner designed to protect o
                <span className="_ _1" />
                ur rights and property and to
                <span className="_ _1" /> facilitate the proper functioning
                <span className="_ _1" /> of the Services.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h4 yff ff3 fs2 fc0 sc0 lsd ws0">
              14. PRIVACY POLICY
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y100 ff4 fs1 fc1 sc0 ls4 ws0">
              We care about d
              <span className="_ _0" />
              ata privacy an
              <span className="_ _0" />
              d security. P
              <span className="_ _0" />
              lease review o
              <span className="_ _0" />
              ur Privacy
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y101 ff4 fs1 fc1 sc0 ls8 ws0">
              Policy:{" "}
              <span className="ff2 fc2 ls2">
                http://www.iberrest.com/privacy
              </span>
              <span className="ls6">
                . By using the Services, you agree to be bound by ou
                <span className="_ _1" />
                r Privacy Policy, which is inc
                <span className="_ _1" />
                orporated into these{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y102 ff4 fs1 fc1 sc0 ls9 ws0">
              Legal Terms. Please be advised the Services are hosted in I
              <span className="_ _0" />
              ndonesia. If you access the Services from any other regi
              <span className="_ _0" />
              on of the{" "}
              <span className="ls5">
                world with l
                <span className="_ _0" />
                aws or{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y103 ff4 fs1 fc1 sc0 ls9 ws0">
              other requirements governing personal data collection,{" "}
              <span className="_ _0" />
              use, or disclosure that differ from appli
              <span className="_ _0" />
              cable laws in Indonesia, then through{" "}
              <span className="lsb">your </span>
            </div>
            <div className="t m0 x1 h5 y104 ff4 fs1 fc1 sc0 lsb ws0">
              continued use of the Services, you are transferri
              <span className="_ _0" />
              ng your data to Indonesia, and you expressly consent to{" "}
              <span className="_ _0" />
              have your data trans
              <span className="ls1">
                f<span className="ls9">erred to and </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y105 ff4 fs1 fc1 sc0 ls9 ws0">
              processed in Indonesia.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y106 ff3 fs2 fc0 sc0 lsd ws0">
              15. TERM AND TERMINATION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y107 ff4 fs1 fc1 sc0 ls2 ws0">
              These Legal Terms shall remain in full f
              <span className="_ _0" />
              orce and effect while you use the Servi
              <span className="_ _0" />
              ces. WITHOUT LIMITING ANY OTHER PROVISION OF T
              <span className="ls5">HESE </span>
            </div>
            <div className="t m0 x1 h5 y108 ff4 fs1 fc1 sc0 ls9 ws0">
              LEGAL TERMS, WE RESERVE THE RIGHT TO, IN OUR SOLE DISCRETION AND
              WITHOUT NOTICE OR LIABILITY, DENY ACCESS TO{" "}
            </div>
            <div className="t m0 x1 h5 y109 ff4 fs1 fc1 sc0 ls8 ws0">
              AND USE OF THE SE
              <span className="ls5">
                RVICES (INCLUDING BLOCKING <span className="_ _0" />
                CERTAIN IP ADDRESSES), TO ANY
                <span className="_ _0" /> PERSON FOR ANY REASON OR FOR NO{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y10a ff4 fs1 fc1 sc0 ls5 ws0">
              REASON, INCLUDING WITHOUT LIMITATI
              <span className="_ _0" />
              ON FOR BREACH OF ANY REPRESENTATION,
              <span className="_ _0" /> WARRANTY, OR COVENANT CONTAINED IN{" "}
            </div>
            <div className="t m0 x1 h5 y10b ff4 fs1 fc1 sc0 ls2 ws0">
              THESE LEGAL TERMS OR OF ANY APPLICABLE LAW OR REGULATION. WE{" "}
              <span className="lse">
                MAY TERMINATE YOUR USE OR <span className="_ _0" />
                PARTICIPATION IN THE{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y10c ff4 fs1 fc1 sc0 ls8 ws0">
              SERVICES OR DELETE YOUR ACCOUNT AND ANY CONTENT OR INFORMATION
              THAT YOU POSTED AT ANY TIME, WITHOUT{" "}
            </div>
            <div className="t m0 x1 h5 y10d ff4 fs1 fc1 sc0 ls4 ws0">
              WARNING, IN OUR S
              <span className="_ _0" />
              OLE DISCRETION.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y10e ff4 fs1 fc1 sc0 ls6 ws0">
              If we terminate or suspend y
              <span className="_ _1" />
              our account for any reason
              <span className="_ _1" />
              , you are prohibited from reg
              <span className="_ _1" />
              istering and creating a new a
              <span className="_ _1" />
              ccount under <span className="lsb">your name, a fake </span>
            </div>
            <div className="t m0 x1 h5 y10f ff4 fs1 fc1 sc0 ls9 ws0">
              or borrowed name, or the name of any third party, even{" "}
              <span className="_ _0" />
              if you may be acting on behalf of the thi
              <span className="_ _0" />
              rd party. In addi
              <span className="ls6">tion to terminating or suspending </span>
            </div>
            <div className="t m0 x1 h5 y110 ff4 fs1 fc1 sc0 lsb ws0">
              your account, we reserve the right to take appropri
              <span className="_ _0" />
              ate legal action, including without li
              <span className="_ _0" />
              mitation pursuing civil, criminal, a
              <span className="ls9">
                nd injunctive redress.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h4 y111 ff3 fs2 fc0 sc0 lsd ws0">
              16. MODIFICATIONS AND INTERRUPTIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y112 ff4 fs1 fc1 sc0 ls4 ws0">
              We reserve the <span className="_ _0" />
              right to cha
              <span className="_ _0" />
              nge, modify, <span className="_ _0" />
              or remove the <span className="_ _0" />
              contents of t
              <span className="_ _0" />
              he Services at
              <span className="_ _0" /> any time or <span className="_ _0" />
              for any reason
              <span className="_ _0" /> at our sole <span className="_ _0" />
              disc
              <span className="lsa">retion without notice. </span>
            </div>
            <div className="t m0 x1 h5 y113 ff4 fs1 fc1 sc0 ls5 ws0">
              However, we have no obli
              <span className="_ _0" />
              gation to update any <span className="_ _0" />
              information on our Ser
              <span className="_ _0" />
              vices. We also reserve <span className="_ _0" />
              the right to modif
              <span className="lsb">
                y or
                <span className="_ _0" /> discontinue all or part of the{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y114 ff4 fs1 fc1 sc0 ls8 ws0">
              Services without notice at <span className="_ _0" />
              any time. We will not
              <span className="ff6 fs4 fc0 ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y115 ff4 fs1 fc1 sc0 ls9 ws0">
              be liable to you or any third party for
              <span className="_ _0" /> any modification, price change,
              suspension, or discont
              <span className="_ _0" />
              inuance of the Services.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y116 ff4 fs1 fc1 sc0 ls4 ws0">
              We cannot guaran
              <span className="_ _0" />
              tee the Servi
              <span className="_ _0" />
              ces will be a
              <span className="_ _0" />
              vailable at <span className="_ _0" />
              all times. We
              <span className="_ _0" /> may experience <span className="_ _0" />
              hardware, soft
              <span className="_ _0" />
              ware, or othe
              <span className="_ _0" />r problems or <span className="_ _0" />n
              <span className="ls9">eed to perform </span>
            </div>
            <div className="t m0 x1 h5 y117 ff4 fs1 fc1 sc0 lse ws0">
              maintenance relat
              <span className="_ _0" />
              ed to the Servi
              <span className="_ _0" />
              ces, resulting <span className="_ _0" />
              in interruptio
              <span className="_ _0" />
              ns, delays, or <span className="_ _0" />
              errors. We reser
              <span className="_ _0" />
              ve the right to
              <span className="_ _0" /> change,{" "}
              <span className="lsa">revise, update, suspend, </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="7" id="pf7">
          <div className="pc pc7 w0 h0">
            <div className="t m0 x1 h5 y35 ff4 fs1 fc1 sc0 ls9 ws0">
              discontinue, or otherwise modify the Services at any{" "}
              <span className="_ _0" />
              time or for any reason without notice to you.
              <span className="_ _0" /> You agree that we have no{" "}
              <span className="ls6">liability whatsoever </span>
            </div>
            <div className="t m0 x1 h5 y36 ff4 fs1 fc1 sc0 ls6 ws0">
              for any loss, damage, or inco
              <span className="_ _1" />
              nvenience caused by yo
              <span className="_ _1" />
              ur inability to access or use th
              <span className="_ _1" />
              e Services during any dow
              <span className="_ _1" />
              ntime or discontinuance of the{" "}
            </div>
            <div className="t m0 x1 h5 y64 ff4 fs1 fc1 sc0 ls8 ws0">
              Services. Nothing in these L
              <span className="_ _0" />
              egal Terms will be construed t
              <span className="_ _0" />o obligate us to maintain{" "}
              <span className="_ _0" />
              and support the Services or <span className="_ _0" />
              to supply any
              <span className="ls1">
                {" "}
                <span className="lsb">corrections, </span>
              </span>
            </div>
            <div className="t m0 x1 h5 yb8 ff4 fs1 fc1 sc0 ls9 ws0">
              updates, or releases in connection therewith.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y118 ff3 fs2 fc0 sc0 lsd ws0">
              17. GOVERNING LAW
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 ybd ff4 fs1 fc1 sc0 ls2 ws0">
              These Legal Terms shall be governed by and defined{" "}
              <span className="_ _0" />
              following the laws of Indonesia. Iber Smar
              <span className="_ _0" />t System and yourself irrevocabl
              <span className="lsb">y consent that </span>
            </div>
            <div className="t m0 x1 h5 y119 ff4 fs1 fc1 sc0 ls6 ws0">
              the courts of Indonesia shall h
              <span className="_ _1" />
              ave exclusive jurisdiction to
              <span className="_ _1" /> resolve any dispute which
              <span className="_ _1" /> may arise in connection with
              <span className="_ _1" /> these Legal Terms.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y11a ff3 fs2 fc0 sc0 lsd ws0">
              18. DISPUTE RESOLUTION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h6 y11b ff5 fs3 fc0 sc0 ls12 ws0">
              Informal Ne
              <span className="_ _0" />
              gotiations
              <span className="_ _0" />
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y11c ff4 fs1 fc1 sc0 ls2 ws0">
              To expedite resolution and control the c
              <span className="_ _0" />
              ost of any dispute, controversy, or
              <span className="_ _0" /> claim related to these Legal Terms (each{" "}
              <span className="_ _0" />a "Dispu
              <span className="ls6">te" and collectively, </span>
            </div>
            <div className="t m0 x1 h5 y11d ff4 fs1 fc1 sc0 ls6 ws0">
              the "Disputes") brought by e
              <span className="_ _1" />
              ither you or us (individually, a
              <span className="_ _1" /> "Party" and collectively, the "P
              <span className="_ _1" />
              arties"), the Pa
              <span className="lsa">
                rties agree to first attempt to negotiate any{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y11e ff4 fs1 fc1 sc0 ls5 ws0">
              Dispute (except those <span className="_ _0" />
              Disputes expressly provi
              <span className="_ _0" />
              ded below) informally <span className="_ _0" />
              for at least thir
              <span className="_ _0" />
              ty (30) days before i
              <span className="_ _0" />
              nitiating arbitrat
              <span className="_ _0" />i
              <span className="ls9">on. Such informal </span>
            </div>
            <div className="t m0 x1 h5 y11f ff4 fs1 fc1 sc0 ls9 ws0">
              negotiations commence upon written notice from one Party to{" "}
              <span className="_ _0" />
              the other Party.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h6 y120 ff5 fs3 fc0 sc0 ls13 ws0">
              Binding Arbi
              <span className="_ _1" />
              trati
              <span className="_ _1" />
              on
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y121 ff4 fs1 fc1 sc0 ls8 ws0">
              Any dispute arising out of
              <span className="_ _0" /> or in connection with thes
              <span className="_ _0" />e Legal Terms, including any{" "}
              <span className="_ _0" />
              question regarding its exi
              <span className="_ _0" />
              stence, validity,
              <span className="ls1">
                {" "}
                <span className="ls9">or termination, shall be </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y122 ff4 fs1 fc1 sc0 lsa ws0">
              referred to and finally resolved by the International Commercial
              Arbitration Court under the European Arb
              <span className="_ _1" />
              <span className="ls6">
                itr
                <span className="_ _1" />
                ation Chamber (Belgium, Br
                <span className="_ _1" />
                ussels,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y123 ff4 fs1 fc1 sc0 ls8 ws0">
              Avenue Louise, 146) according
              <span className="_ _0" /> to the Rules of this ICAC, whi
              <span className="_ _0" />
              ch, as a result of refer
              <span className="_ _0" />
              ring to it, is consider
              <span className="_ _0" />
              ed as the part of{" "}
              <span className="ls6">this clause. The number </span>
            </div>
            <div className="t m0 x1 h5 y124 ff4 fs1 fc1 sc0 ls9 ws0">
              of arbitrators shall be three (3). The <span className="_ _0" />
              seat, or legal place, or arbitration shal
              <span className="_ _0" />l be Denpasar, Indonesia. The language of
              the proceedings <span className="_ _0" />
              shall be{" "}
            </div>
            <div className="t m0 x1 h5 y125 ff4 fs1 fc1 sc0 ls8 ws0">
              English. The governing law of
              <span className="_ _0" /> these Legal Terms shall be s
              <span className="_ _0" />
              ubstantive law of Indonesia.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h6 y126 ff5 fs3 fc0 sc0 ls13 ws0">
              Restricti
              <span className="_ _1" />
              ons
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y127 ff4 fs1 fc1 sc0 ls2 ws0">
              The Parties agree that any arbitrati
              <span className="_ _0" />
              on shall be limited to the Dispute between{" "}
              <span className="_ _0" />
              the Parties individually. To the f
              <span className="_ _0" />
              ull extent p<span className="ls9">ermitted by law, (a) no </span>
            </div>
            <div className="t m0 x1 h5 y98 ff4 fs1 fc1 sc0 ls9 ws0">
              arbitration shall be joined with any other
              <span className="_ _0" /> proceeding; (b) there is no right or
              author
              <span className="_ _0" />
              ity for any Dispute to be arbitrated on a{" "}
              <span className="_ _0" />
              class
              <span className="ls1">-</span>
              action basis or to{" "}
            </div>
            <div className="t m0 x1 h5 y128 ff4 fs1 fc1 sc0 ls9 ws0">
              utilize class action procedures; and (c) there{" "}
              <span className="_ _0" />
              is no right or authority for any Dispute <span className="_ _0" />
              to be brought in a purported represen
              <span className="ls6">tative capacity on behalf </span>
            </div>
            <div className="t m0 x1 h5 y9a ff4 fs1 fc1 sc0 ls9 ws0">
              of the general public or any other persons.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h6 y129 ff5 fs3 fc0 sc0 ls10 ws0">
              Exceptions t
              <span className="_ _1" />o Informal <span className="_ _1" />
              Negotiatio
              <span className="_ _1" />
              ns and Arbit
              <span className="_ _1" />
              ration
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y12a ff4 fs1 fc1 sc0 ls2 ws0">
              The Parties agree that the following Dis
              <span className="_ _0" />
              putes are not subject to the above provi
              <span className="_ _0" />
              sions concerning informal negotiations bindi
              <span className="_ _0" />n
              <span className="ls9">g arbitration: (a) any </span>
            </div>
            <div className="t m0 x1 h5 y12b ff4 fs1 fc1 sc0 ls5 ws0">
              Disputes seeking to enf
              <span className="_ _0" />
              orce or protect, or
              <span className="_ _0" /> concerning the validi
              <span className="_ _0" />
              ty of, any of the
              <span className="_ _0" /> intellectual proper
              <span className="_ _0" />
              ty righ
              <span className="ls6">
                ts of a Party; (b) any Dispute related to, or{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y12c ff4 fs1 fc1 sc0 ls9 ws0">
              arising from, allegations of theft, <span className="_ _0" />
              piracy, invasion of privacy, or unauthorized use;{" "}
              <span className="_ _0" />
              and (c) any claim for injunctive relief
              <span className="_ _0" />
              <span className="ls6">. If this provision is found to </span>
            </div>
            <div className="t m0 x1 h5 y12d ff4 fs1 fc1 sc0 ls9 ws0">
              be illegal or unenforceable, then neither Part
              <span className="_ _0" />
              y will elect to arbitrate any Dispute f
              <span className="_ _0" />
              alling within that portion of this <span className="_ _0" />
              provision found to be illegal or{" "}
            </div>
            <div className="t m0 x1 h5 y12e ff4 fs1 fc1 sc0 ls9 ws0">
              unenforceable and such Dispute shall be decided by a court{" "}
              <span className="_ _0" />
              of competent jurisdiction within the courts{" "}
              <span className="_ _0" />
              listed for jurisdiction above, and the <span className="_ _0" />
              Parties{" "}
            </div>
            <div className="t m0 x1 h5 y12f ff4 fs1 fc1 sc0 ls9 ws0">
              agree{" "}
              <span className="ls6">
                to submit to the personal jurisdic
                <span className="_ _1" />
                tion of that court.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h4 y7d ff3 fs2 fc0 sc0 lsd ws0">
              19. CORRECTIONS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y130 ff4 fs1 fc1 sc0 ls2 ws0">
              There may be information on the Services t
              <span className="_ _0" />
              hat contains typographical errors, i
              <span className="_ _0" />
              naccuracies, or omissions, including desc
              <span className="_ _0" />
              riptio
              <span className="ls9">ns, pricing, </span>
            </div>
            <div className="t m0 x1 h5 y131 ff4 fs1 fc1 sc0 ls9 ws0">
              availability, and various other information.
              <span className="_ _0" /> We reserve the right to correct any
              errors, <span className="_ _0" />
              inaccuracies, or
              <span className="ff6 fs4 fc0 ls1"> c</span>
              omissions and to change or update the{" "}
            </div>
            <div className="t m0 x1 h5 y132 ff4 fs1 fc1 sc0 ls6 ws0">
              information on the Services a
              <span className="_ _1" />t any time, without prior notice.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y133 ff3 fs2 fc0 sc0 lsd ws0">
              20. DISCLAIMER
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y84 ff4 fs1 fc1 sc0 ls2 ws0">
              THE SERVICES ARE PROVIDED ON AN AS
              <span className="ls1">
                -<span className="ls6">IS AND AS</span>-
                <span className="ls8">
                  AVAILABLE BASIS. YOU AGREE THAT YOUR USE OF THE SERVICES WILL
                  BE AT{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y134 ff4 fs1 fc1 sc0 ls8 ws0">
              YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, WE
              DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, I
              <span className="_ _0" />N{" "}
            </div>
            <div className="t m0 x1 h5 y135 ff4 fs1 fc1 sc0 ls5 ws0">
              CONNECTION WITH THE SERVICES AND YOUR USE{" "}
              <span className="_ _0" />
              THEREOF, INCLUDING, WITHOUT LIMIT
              <span className="_ _0" />
              ATION, THE IMPLIED WARRANTIES <span className="_ _0" />
              OF{" "}
            </div>
            <div className="t m0 x1 h5 y136 ff4 fs1 fc1 sc0 lse ws0">
              MERCHANTABILITY, FITNESS <span className="_ _0" />
              FOR A PARTICULAR PURPOSE,
              <span className="_ _0" /> AND NON
              <span className="ls1">
                -{" "}
                <span className="ls6">
                  INFRINGEMENT. WE MAKE
                  <span className="_ _1" /> NO WARRANTIES OR
                  <span className="_ _1" />{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y137 ff4 fs1 fc1 sc0 ls5 ws0">
              REPRESENTATIONS ABOUT THE ACCURACY OR COMPL
              <span className="_ _0" />
              ETENESS OF THE SERVICES' CONTENT <span className="_ _0" />
              OR THE CONTENT OF ANY{" "}
            </div>
            <div className="t m0 x1 h5 y86 ff4 fs1 fc1 sc0 ls4 ws0">
              WEBSITES OR MOBIL
              <span className="_ _0" />
              E APPLICATIONS L
              <span className="_ _0" />
              INKED TO THE SERV
              <span className="_ _0" />
              ICES AND WE WILL <span className="_ _0" />
              ASSUME NO LIABIL
              <span className="_ _0" />
              ITY OR RESPONSIBI
              <span className="_ _0" />
              LITY FOR{" "}
            </div>
            <div className="t m0 x1 h5 y138 ff4 fs1 fc1 sc0 ls8 ws0">
              ANY (1) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT AND
              MATERIALS, (2) PERSONAL INJURY OR PROPERTY DAMAGE, OF{" "}
            </div>
            <div className="t m0 x1 h5 y139 ff4 fs1 fc1 sc0 ls8 ws0">
              ANY NATURE WHATSOEVER,{" "}
              <span className="ls5">
                RESULTING FROM YOUR ACCESS TO AND <span className="_ _0" />
                USE OF THE SERVICES, (3) <span className="_ _0" />
                ANY UNAUTHORIZED ACCESS{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y13a ff4 fs1 fc1 sc0 ls2 ws0">
              TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL
              INFORMATION AND/OR FINANCIAL INFORMATION{" "}
            </div>
            <div className="t m0 x1 h5 y13b ff4 fs1 fc1 sc0 ls8 ws0">
              STORED THEREIN, (4) ANY INTERRUPTION OR CESSATION OF TRANSMISSION
              TO OR{" "}
              <span className="ls2">
                FROM THE SERVI
                <span className="_ _0" />
                CES, (5) ANY BUGS,{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y13c ff4 fs1 fc1 sc0 ls8 ws0">
              VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR
              THROUGH THE SERVICES BY ANY THIRD PARTY,{" "}
            </div>
            <div className="t m0 x1 h5 y13d ff4 fs1 fc1 sc0 ls8 ws0">
              AND/OR (6) ANY ERRORS OR OMISSIONS IN ANY CONTENT AND MATERIALS OR
              FOR ANY LOSS OR DAMAGE OF ANY KIND{" "}
            </div>
            <div className="t m0 x1 h5 y32 ff4 fs1 fc1 sc0 ls6 ws0">
              INCURRED AS A
              <span className="ls1">
                {" "}
                <span className="ls5">
                  RESULT OF THE USE OF ANY CONTENT POSTED, TRANSMITTED, OR OTHE
                  <span className="_ _0" />
                  RWISE MADE AVAILABLE VIA THE{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y13e ff4 fs1 fc1 sc0 ls8 ws0">
              SERVICES. WE DO NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME
              RESPONSIBILITY FOR ANY PRODUCT OR SERVICE{" "}
            </div>
            <div className="t m0 x1 h5 y13f ff4 fs1 fc1 sc0 ls8 ws0">
              ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SERVICES, ANY
              <span className="ls1">
                {" "}
                <span className="ls5">
                  HYPERLINKED WEB
                  <span className="_ _0" />
                  SITE, OR ANY WEBSITE OR{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y140 ff4 fs1 fc1 sc0 lse ws0">
              MOBILE APPLICATION FEATUR
              <span className="_ _0" />
              ED IN ANY BANNER OR OTHER
              <span className="_ _0" /> ADVERTISING, AND WE WIL
              <span className="_ _0" />L NOT BE A PARTY TO OR{" "}
              <span className="_ _0" />
              IN ANY WAY BE{" "}
            </div>
            <div className="t m0 x1 h5 y141 ff4 fs1 fc1 sc0 ls5 ws0">
              RESPONSIBLE FOR MONITORING ANY TRANSACT
              <span className="_ _0" />
              ION BETWEEN YOU AND ANY THIRD
              <span className="ls1">
                -<span className="ls8">PARTY PROVIDERS OF PRODUCTS OR </span>
              </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="8" id="pf8">
          <div className="pc pc8 w0 h0">
            <div className="t m0 x1 h5 y35 ff4 fs1 fc1 sc0 ls8 ws0">
              SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY
              MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD{" "}
            </div>
            <div className="t m0 x1 h5 y36 ff4 fs1 fc1 sc0 ls5 ws0">
              USE YOUR BEST JUDGMENT AND EXERCISE <span className="_ _0" />
              CAUTION WHERE APPROPRIATE.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y142 ff3 fs2 fc0 sc0 lsd ws0">
              21. LIMITATIONS OF LIABILITY
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y143 ff4 fs1 fc1 sc0 ls6 ws0">
              IN NO EVENT WILL WE O
              <span className="_ _1" />
              R OUR DIRECTORS, E
              <span className="_ _1" />
              MPLOYEES, OR AGENT
              <span className="_ _1" />
              S BE LIABLE TO YOU OR A
              <span className="_ _1" />
              NY THIRD PARTY FOR
              <span className="_ _1" /> ANY DIRECT,{" "}
            </div>
            <div className="t m0 x1 h5 y3 ff4 fs1 fc1 sc0 ls6 ws0">
              INDIRECT, CONSEQUE
              <span className="_ _1" />
              NTIAL, EXEMPLARY, INC
              <span className="_ _1" />
              IDENTAL, SPECIAL, OR PU
              <span className="_ _1" />
              NITIVE DAMAGES, INCL
              <span className="_ _1" />
              UDING LOST PROFIT, LO
              <span className="_ _1" />
              ST{" "}
            </div>
            <div className="t m0 x1 h5 ybd ff4 fs1 fc1 sc0 ls5 ws0">
              REVENUE, LOSS OF DATA, OR OTHER <span className="_ _0" />
              DAMAGES
              <span className="ls1">
                {" "}
                <span className="ls8">
                  ARISING FROM YOUR USE OF THE SERVICES, EVEN IF WE HAVE BEEN
                  ADVISED{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y119 ff4 fs1 fc1 sc0 ls5 ws0">
              OF THE POSSIBILITY OF SUCH <span className="_ _0" />
              DAMAGES. NOTWITHSTANDING ANYTHING TO THE
              <span className="_ _0" /> CONTRARY CONTAINED HEREIN, OUR LIA
              <span className="_ _0" />
              BILITY{" "}
            </div>
            <div className="t m0 x1 h5 y144 ff4 fs1 fc1 sc0 ls2 ws0">
              TO YOU FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE FORM OF THE
              ACTION, WILL
              <span className="ls1">
                {" "}
                <span className="ls8">
                  AT ALL TIMES BE LIMITED TO $1.00
                </span>{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y145 ff4 fs1 fc1 sc0 ls5 ws0">
              USD. CERTAIN US STATE LAWS AND <span className="_ _0" />
              INTERNATIONAL LAWS DO NOT ALLOW LI
              <span className="_ _0" />
              MITATIONS ON IMPLIED WARRANTIES OR <span className="_ _0" />
              THE{" "}
            </div>
            <div className="t m0 x1 h5 y146 ff4 fs1 fc1 sc0 ls8 ws0">
              EXCLUSION OR LIMITATION OF CERTAIN DAMAGES. IF THESE LAWS APPLY TO
              YOU, SOME OR ALL OF THE ABOVE DISCLAIMERS{" "}
            </div>
            <div className="t m0 x1 h5 y147 ff4 fs1 fc1 sc0 ls5 ws0">
              OR LIMITATIONS MAY NOT APPLY T
              <span className="_ _0" />
              O YOU, AND YOU MAY HAVE ADDITIONAL RI
              <span className="_ _0" />
              GHTS.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y148 ff3 fs2 fc0 sc0 lsd ws0">
              22. INDEMNIFICATION
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h5 y149 ff4 fs1 fc1 sc0 ls8 ws0">
              You agree to defend, indemnif
              <span className="_ _0" />
              y, and hold us harmless, in
              <span className="_ _0" />
              cluding our subsidiaries, <span className="_ _0" />
              affiliates, and all of <span className="_ _0" />
              our respective offic
              <span className="ls9">ers, agents, partners, </span>
            </div>
            <div className="t m0 x1 h5 yc5 ff4 fs1 fc1 sc0 ls9 ws0">
              and employees, from and against any loss, damage, liabili
              <span className="_ _0" />
              ty, claim, or demand, including reasonable attorney
              <span className="_ _0" />
              <span className="lsb">s’ fees and expenses, made by any </span>
            </div>
            <div className="t m0 x1 h5 y14a ff4 fs1 fc1 sc0 ls6 ws0">
              third party due to or arising ou
              <span className="_ _1" />
              t of: (1) your Contributions; (2) u
              <span className="_ _1" />
              se of the Services; (3) breac
              <span className="_ _1" />h of these Legal Terms; (4) a
              <span className="ls9">ny breach of your </span>
            </div>
            <div className="t m0 x1 h5 y14b ff4 fs1 fc1 sc0 lsa ws0">
              representations and warranties set forth in these Legal Terms; (5)
              your violati
              <span className="_ _1" />
              <span className="ls9">
                on of the rights of a third party, including but{" "}
                <span className="_ _0" />
                not limited to intellectual{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 y14c ff4 fs1 fc1 sc0 ls9 ws0">
              property rights; or (6) any overt harmful act
              <span className="_ _0" /> toward any other user of the Services
              with whom you connected <span className="_ _0" />
              via the Services.
              <span className="ls1">
                {" "}
                <span className="ls5">
                  Notwithstandi
                  <span className="_ _0" />
                  ng the{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y14d ff4 fs1 fc1 sc0 ls6 ws0">
              foregoing, we reserve the rig
              <span className="_ _1" />
              ht,{" "}
              <span className="ls9">
                at your expense, to assume the exclusive defense and control
                <span className="_ _0" /> of any matter for which you are
                required to{" "}
              </span>
            </div>
            <div className="t m0 x1 h5 yf6 ff4 fs1 fc1 sc0 ls6 ws0">
              indemnify us, and you agre
              <span className="_ _1" />
              e to cooperate, at your expe
              <span className="_ _1" />
              nse, with our defense of suc
              <span className="_ _1" />
              h claims. We will use reason
              <span className="_ _1" />
              able efforts to
              <span className="ls1">
                {" "}
                <span className="ls9">notify you of any </span>
              </span>
            </div>
            <div className="t m0 x1 h5 y14e ff4 fs1 fc1 sc0 lsb ws0">
              such cl
              <span className="ls9">
                aim, action, or proceeding which is subject t
                <span className="_ _0" />o this indemnification upon becoming
                aware of it.
                <span className="ls1"> </span>
              </span>
            </div>
            <div className="t m0 x1 h4 yf8 ff3 fs2 fc0 sc0 lsd ws0">
              23. USER DATA
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y14f ff2 fs1 fc1 sc0 ls4 ws0">
              We will mainta
              <span className="_ _0" />
              in certain dat
              <span className="_ _0" />
              a that you tr
              <span className="_ _0" />
              ansmit to the <span className="_ _0" />
              Services for t
              <span className="_ _0" />
              he purpose of <span className="_ _0" />
              managing the per
              <span className="_ _0" />
              formance of the
              <span className="_ _0" /> Services,{" "}
              <span className="ls9">as well as </span>
            </div>
            <div className="t m0 x1 h3 y150 ff2 fs1 fc1 sc0 ls2 ws0">
              data relating to your use of the Services.{" "}
              <span className="_ _0" />
              Although we perform regular routine backups of data, you{" "}
              <span className="_ _0" />
              are solely responsib
              <span className="ls6">le for all data </span>
            </div>
            <div className="t m0 x1 h3 y151 ff2 fs1 fc1 sc0 lsa ws0">
              that you transmit or that relates to any activity you have un
              <span className="_ _1" />
              dertaken using the Services. You agree that we sh
              <span className="_ _1" />
              all have no lia
              <span className="ls2">bility to you </span>
            </div>
            <div className="t m0 x1 h3 y152 ff2 fs1 fc1 sc0 lsa ws0">
              for any loss or corruption of any such data, and
              <span className="_ _1" /> you hereby waive any right of action
              against us aris
              <span className="_ _1" />
              <span className="ls6">
                ing from any such loss
                <span className="_ _1" /> or{" "}
              </span>
            </div>
            <div className="t m0 x1 h3 y99 ff2 fs1 fc1 sc0 ls9 ws0">
              corruption of such data.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y153 ff3 fs2 fc0 sc0 lsd ws0">
              24. ELECTRONIC COMMUNICATIONS, TRANSACTIONS, AND SIGNATURES
              <span className="_ _0" />
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y154 ff2 fs1 fc1 sc0 ls8 ws0">
              Visiting the Services, sending us <span className="_ _0" />
              emails, and completing online forms consti
              <span className="_ _0" />
              tute electronic communications. You consent
              <span className="_ _0" /> to re
              <span className="ls2">ceive </span>
            </div>
            <div className="t m0 x1 h3 y155 ff2 fs1 fc1 sc0 ls9 ws0">
              electronic communications, and you agree that all agreements, noti
              <span className="_ _0" />
              ces, disclosures, and other communications we provide to yo
              <span className="ls2">u </span>
            </div>
            <div className="t m0 x1 h3 y156 ff2 fs1 fc1 sc0 ls9 ws0">
              electronically, via email and on the Services, sat
              <span className="_ _0" />
              isfy any legal requirement that such communication be in writi
              <span className="_ _0" />
              ng. YOU HEREB
              <span className="ls8">Y </span>
            </div>
            <div className="t m0 x1 h3 y157 ff2 fs1 fc1 sc0 ls5 ws0">
              AGREE TO THE USE OF ELECTRONIC SI
              <span className="_ _0" />
              GNATURES, CONTRACTS, ORDERS, AND OTHER RECORDS
              <span className="_ _0" />, AND TO ELECTRONIC{" "}
            </div>
            <div className="t m0 x1 h3 y158 ff2 fs1 fc1 sc0 ls5 ws0">
              DELIVERY OF NOTICES, POLICIE
              <span className="ls8">
                S, AND RECORDS OF TRANSACTIONS INITIATED OR COMPLETED BY US OR
                VIA <span className="_ _0" />
                THE SERVICES.{" "}
              </span>
            </div>
            <div className="t m0 x1 h3 y104 ff2 fs1 fc1 sc0 ls8 ws0">
              You hereby waive any rights or requirement
              <span className="_ _0" />
              s under any statutes, regulations,
              <span className="_ _0" /> rules, ordinances, or other laws i
              <span className="_ _0" />n any jurisdic
              <span className="lsa">tion which </span>
            </div>
            <div className="t m0 x1 h3 y159 ff2 fs1 fc1 sc0 ls14 ws0">
              require an original signature or delivery or retention of non
              <span className="ls1">
                -
                <span className="ls9">
                  electronic records, or to payments or the granting of credits
                  by any means{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h3 y15a ff2 fs1 fc1 sc0 ls2 ws0">
              other than electronic means.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y15b ff3 fs2 fc0 sc0 lsd ws0">
              25. CALIFORNIA USERS AND RESIDENTS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y15c ff2 fs1 fc1 sc0 ls6 ws0">
              If any complaint with us is n
              <span className="_ _1" />
              ot satisfactorily resolved, y
              <span className="_ _1" />
              ou can contact the Com
              <span className="_ _1" />
              plaint Assistance Unit of th
              <span className="_ _1" />e Division of Con
              <span className="ls9">sumer </span>
            </div>
            <div className="t m0 x1 h3 y15d ff2 fs1 fc1 sc0 ls8 ws0">
              Services of the California Department
              <span className="_ _0" /> of Consumer Affairs in writing at 16
              <span className="_ _0" />
              25 North Market Blvd., Suite N 112, <span className="_ _0" />
              Sacramento, Cal
              <span className="ls6">ifornia </span>
            </div>
            <div className="t m0 x1 h3 y15e ff2 fs1 fc1 sc0 ls9 ws0">
              95834 or by telephone at (800) 952
              <span className="ls1">-</span>
              5210 or (916) 445
              <span className="ls1">-</span>
              1254.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y15f ff3 fs2 fc0 sc0 lsd ws0">
              26. MISCELLANEOUS
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y160 ff2 fs1 fc1 sc0 ls2 ws0">
              These Legal Terms and any policies or operating rules post
              <span className="_ _0" />
              ed by us on the Services or in respect to the{" "}
              <span className="_ _0" />
              Services constitute the entire{" "}
            </div>
            <div className="t m0 x1 h3 y135 ff2 fs1 fc1 sc0 ls9 ws0">
              agreement and understanding between you and us. Our failure to
              exerci
              <span className="_ _0" />
              se or enforce any right or provision of these Legal{" "}
              <span className="ls2">
                Terms shall
                <span className="_ _0" />{" "}
              </span>
            </div>
            <div className="t m0 x1 h3 y161 ff2 fs1 fc1 sc0 ls2 ws0">
              not operate as a waiver of such right or provi
              <span className="_ _0" />
              sion. These Legal Terms operate to the fullest ext
              <span className="_ _0" />
              ent permissible by law. We ma
              <span className="ls9">y assign </span>
            </div>
            <div className="t m0 x1 h3 y162 ff2 fs1 fc1 sc0 ls9 ws0">
              any or all of our rights and obligations to{" "}
              <span className="_ _0" />
              others at any time. We shall not be responsible or l
              <span className="_ _0" />
              iable for any{" "}
              <span className="ls6">loss, damage, delay, or failure </span>
            </div>
            <div className="t m0 x1 h3 y163 ff2 fs1 fc1 sc0 lsa ws0">
              to act caused by any cause beyond our reas
              <span className="_ _1" />
              onable control. If any provision or part of a provision o
              <span className="_ _1" />f these Legal Terms is det
              <span className="ls9">ermined to </span>
            </div>
            <div className="t m0 x1 h3 y164 ff2 fs1 fc1 sc0 ls2 ws0">
              be unlawful, void, or unenforceable, that pr
              <span className="_ _0" />
              ovision or part of the provision is deemed s
              <span className="ls9">
                everable f
                <span className="_ _0" />
                rom these Legal Terms and does not{" "}
              </span>
            </div>
            <div className="t m0 x1 h3 y165 ff2 fs1 fc1 sc0 ls9 ws0">
              affect the validity and enforceability of any r
              <span className="_ _0" />
              emaining provisions. There is no joint venture, part
              <span className="_ _0" />
              nership, employment or age
              <span className="ls2">ncy </span>
            </div>
            <div className="t m0 x1 h3 y166 ff2 fs1 fc1 sc0 ls14 ws0">
              relationship created between you and us as a result of these Legal
              Terms or use of
              <span className="ls1">
                {" "}
                <span className="lsa">
                  the Serv
                  <span className="_ _1" />
                  ices. You agree that these Legal Terms will{" "}
                </span>
              </span>
            </div>
            <div className="t m0 x1 h3 y13b ff2 fs1 fc1 sc0 ls2 ws0">
              not be construed against us by virtue of having{" "}
              <span className="_ _0" />
              drafted them. You hereby waive any and all defenses you may have{" "}
              <span className="_ _0" />
              based on the
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y167 ff2 fs1 fc1 sc0 ls9 ws0">
              electronic form of these Legal Terms and the lack of signi
              <span className="_ _0" />
              ng by the parties hereto to execute these Legal Terms.
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h4 y168 ff3 fs2 fc0 sc0 lsd ws0">
              27. CONTACT US
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y169 ff2 fs1 fc1 sc0 ls6 ws0">
              In order to resolve a comp
              <span className="_ _1" />
              laint regarding the Services
              <span className="_ _1" /> or to receive further inform
              <span className="_ _1" />
              ation regarding use of the
              <span className="_ _1" /> Services, pleas
              <span className="ls9">e contact us </span>
            </div>
            <div className="t m0 x1 h3 y16a ff2 fs1 fc1 sc0 ls9 ws0">
              at:
              <span className="ls1"> </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
        <div className="pf w0 h0" data-page-no="9" id="pf9">
          <div className="pc pc9 w0 h0">
            <div className="t m0 x1 h3 y35 ff2 fs1 fc1 sc0 ls6 ws0">
              Iber Smart System
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y16b ff2 fs1 fc1 sc0 ls9 ws0">
              Jl. Gn. Krakatau No.25, Pemecutan Klod, Kec. Denpasar Bar., Kota
              Denpasar,
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y16c ff2 fs1 fc1 sc0 ls5 ws0">
              Bali 80112, Indonesia
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y16d ff2 fs1 fc1 sc0 ls5 ws0">
              Denpasar, Bali 80112
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y16e ff2 fs1 fc1 sc0 ls6 ws0">
              Indonesia
              <span className="ls1"> </span>
            </div>
            <div className="t m0 x1 h3 y16f ff2 fs1 fc1 sc0 ls6 ws0">
              info@iberrest.com
              <span className="ls1"> </span>
            </div>
          </div>
          <div
            className="pi"
            data-data='{"ctm":[1.500000,0.000000,0.000000,1.500000,0.000000,0.000000]}'
          />
        </div>
      </div>
      <div className="loading-indicator"></div>
    </>
  );
};

export default Terms;
