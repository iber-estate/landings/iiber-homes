import localFont from 'next/font/local';

export const satoshi = localFont({
  src: [
    {
      path: './public/fonts/Satoshi-Light.woff2',
      weight: '300',
      style: 'normal',
    },
    {
      path: './public/fonts/Satoshi-LightItalic.woff2',
      weight: '300',
      style: 'italic',
    },
    {
      path: './public/fonts/Satoshi-Regular.woff2',
      weight: '400',
      style: 'normal',
    },
    {
      path: './public/fonts/Satoshi-Medium.woff2',
      weight: '500',
      style: 'normal',
    },
    {
      path: './public/fonts/Satoshi-MediumItalic.woff2',
      weight: '500',
      style: 'italic',
    },
    {
      path: './public/fonts/Satoshi-Bold.woff2',
      weight: '600',
      style: 'normal',
    },
    {
      path: './public/fonts/Satoshi-BoldItalic.woff2',
      weight: '600',
      style: 'italic',
    },
    {
      path: './public/fonts/Satoshi-Black.woff2',
      weight: '700',
      style: 'normal',
    },
    {
      path: './public/fonts/Satoshi-BlackItalic.woff2',
      weight: '700',
      style: 'italic',
    },
  ],
  display: 'swap',
  variable: '--font-satoshi',
});

export const onest = localFont({
  src: [
    {
      path: './public/fonts/OnestThin1602-hint.woff',
      weight: '100',
      style: 'thin',
    },
    {
      path: './public/fonts/OnestLight1602-hint.woff',
      weight: '200',
      style: 'light',
    },
    {
      path: './public/fonts/OnestRegular1602-hint.woff',
      weight: '300',
      style: 'regular',
    },
    {
      path: './public/fonts/OnestMedium1602-hint.woff',
      weight: '400',
      style: 'medium',
    },
    {
      path: './public/fonts/OnestBold1602-hint.woff',
      weight: '500',
      style: 'bold',
    },
    {
      path: './public/fonts/OnestExtraBold1602-hint.woff',
      weight: '600',
      style: 'extra-bold',
    },
    {
      path: './public/fonts/OnestBlack1602-hint.woff',
      weight: '700',
      style: 'black',
    },
  ],
  display: 'swap',
  variable: '--font-onest',
});
